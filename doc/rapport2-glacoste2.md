# Rapport individuel d'activité

de Guillaume Lacoste

## Première itération

| Tâche                                                        | Issue                                                 | Degré d'avancement | Commentaire                                                  |
| ------------------------------------------------------------ | ----------------------------------------------------- | ------------------ | ------------------------------------------------------------ |
| Définition du cahier des charges et des user stories         | [#6](https://gitlab.com/Ato0m3/langfall/-/issues/6)   | Terminé.           | Fait avec toute l'équipe.                                    |
| Conception de l'architecture du projet                       | [#14](https://gitlab.com/Ato0m3/langfall/-/issues/14) | Terminé.           | Fait avec toute l'équipe.                                    |
| Mise en place du GitLab                                      | [#15](https://gitlab.com/Ato0m3/langfall/-/issues/15) | Terminé.           | Mise en place du gitlab côté gestion de projet(board, issues, milestone, tag...) et côté dépôt (branche, merge Request), puis présentation à l'équipe du fonctionnement. |
| Environnement java                                           | [#16](https://gitlab.com/Ato0m3/langfall/-/issues/16) | Terminé.           | Mise en place de l'environnement de travail java (création et configuration du projet gradle, mise en place de fichier de code Style pour le projet ), puis présentation à l'équipe |
| Le joueur peut voir la carte                                 | [#26](https://gitlab.com/Ato0m3/langfall/-/issues/26) | Terminé.           | Première US, mise en place des bases du projet (modèle,view ...) , création d'un système de templates de carte à créer/choisir , création d'un ImageLoader chargant les images (présentes dans les ressources) au début de l'application |
| Le joueur peut déplacer son personnage sur la carte          | [#18](https://gitlab.com/Ato0m3/langfall/-/issues/18) | Terminé.           | Mise en place durant l'US de notre implémentation du patron listener, afin de faire réagir l'UI au changement des données présentes dans le modèle (package langfall.event) |
| Le joueur peut récolter des ressources sur les différents terrains de la carte | [#25](https://gitlab.com/Ato0m3/langfall/-/issues/25) | Terminé.           | Support sur l'US réalisé par Marine Blasius : affichage des ressources du joueur + affichage de la durabilité d'un terrain (tooltip) |
| Livraison V1                                                 | [#10](https://gitlab.com/Ato0m3/langfall/-/issues/10) | Terminé.           | Ecriture du script sh pour lancer Langfall sur Ubuntu (script pour Windows réalisé par Valentin Hou) , rédaction du rapport et du manuel utilisateur (avec l'équipe) et génération des livrables. |







## Seconde itération

| Tâche                                                       | Issue                                                 | Degré d'avancement | Commentaire                                                  |
| ----------------------------------------------------------- | ----------------------------------------------------- | ------------------ | ------------------------------------------------------------ |
| Le joueur peut sauvegarder sa partie en cours               | [#27](https://gitlab.com/Ato0m3/langfall/-/issues/27) | Terminé.           | Mise en place du système de sauvegarde de partie pour le jeu. Utilisation/Configuration de GSON afin de désérialiser et sérialiser les données. |
| Le joueur peut utiliser les batiments présents sur la carte | [#23](https://gitlab.com/Ato0m3/langfall/-/issues/23) | 80%                | Fait avec Marine. Modèle fait mais la partie vue est reportée à la prochaine itération. (mandatory manquante pour réaliser cette partie) |
| Game title                                                  | [#39](https://gitlab.com/Ato0m3/langfall/-/issues/39) | Terminé.           | Amélioration -> regex pour le titre d'une partie.            |
| Durabilité eau                                              | [#38](https://gitlab.com/Ato0m3/langfall/-/issues/38) | Terminé.           | Amélioration. -> durabilité de l'eau infini                  |
| Livraison V2                                                | [#11](https://gitlab.com/Ato0m3/langfall/-/issues/11) | Terminé.           | Générations des livreables.                                  |

## Troisième itération

| Tâche                                                        | Issue                                                 | Degré d'avancement | Commentaire                                                  |
| ------------------------------------------------------------ | ----------------------------------------------------- | ------------------ | ------------------------------------------------------------ |
| Le joueur peut utiliser les batiments présents sur la carte (suite) | [#23](https://gitlab.com/Ato0m3/langfall/-/issues/23) | Terminé.           | Fin de l'US  fait avec Marine. Réalisation de la partie view de l'US. |
| Le joueur peut choisir les associations touches clavier/actions | [#41](https://gitlab.com/Ato0m3/langfall/-/issues/41) | Terminé.           | Fait avec Rémi. Nous avons rendu les bindings des touches d'actions dynamique en fonction de ceux rentrés par l'utilisateur dans une fenêtre ou bien ceux par défaut. |
| Les ressources d'une case se régénèrent                      | [#36](https://gitlab.com/Ato0m3/langfall/-/issues/36) | Terminé.           | Implémentation du système de tâches de fond, et création de la tâche de régénération des ressources |
| Confirmation avant de quitter                                | [#47](https://gitlab.com/Ato0m3/langfall/-/issues/47) | Terminé.           | Amélioration -> Ajout d'un dialogue demander une confirmation avant de quitter (demande de sauvegarder) |
| Livraison V3                                                 | [#12](https://gitlab.com/Ato0m3/langfall/-/issues/12) | Terminé.           | Rédaction du rapport + livrables.                            |
| Préparation Oral fin de projet                               | [#17](https://gitlab.com/Ato0m3/langfall/-/issues/17) | Terminé.           |                                                              |



