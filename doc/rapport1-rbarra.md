Rapport individuel d'activité

Fait par Rémi Barra


| Tâche                                                        | Issue | Degré d'avancement | Commentaire                         |
| ------------------------------------------------------------ | ----- | ------------------ | ----------------------------------- |
| Définition du cahier des charges et des user stories         | [#6]  | Terminé            | Fait avec toute l'équipe            |
| Conception de l'architecture du projet                       | [#14] | Terminé            | Fait avec toute l'équipe            |
| Aide à la rédaction des différents documents de la V1        | [#10] | Terminé            | Fait avec une partie de l'équipe    |
