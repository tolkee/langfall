<div align="center">
    <img alt="langfall" src="assets/logo.png" width="80" />
    <h1>
  Manuel utilisateur
</h1>
</div>

## Quelle version de java faut-il pour lancer le jeu ?

Une version de Java supérieure ou égale à 11 est requise (JDK ou JRE).

## Comment lancer le jeu?

Pour lancer le jeu, il faut executer le launcher fourni avec le .jar. (le laucher doit se trouver dans le même répertoire que le .jar). Afin que les sauvegardes du jeu fonctionnent correctement, **il faut exécuter le jar dans le même dossier où se trouve le dossier _backup_** (il est généré lors de la première sauvegarde du jeu).

Voici les différents launcher en fonction de votre système d'exploitation:

- **launcher.bat** si l'utilisateur est sous Windows.
- **launcher.sh** si l'utilisateur est sous Linux.

Il est également possible de lancer le jeu depuis un terminal.
Il faut alors ouvrir un terminal à partir du dossier contant le .jar puis exécuter la commande suivante:

`java -jar nom_du_jar`

> Note: Remplacer nom_du_jar par le nom du fichier jar qui vous a été fourni (exemple pour l'itération 1 : **source1.jar**).

## Le jeu

### Démarrer une partie 

Au premier lancement du jeu, l'écran d'accueil est ainsi :

![](assets/welcome_screen.png)

Il faut alors saisir le nom de le nouvelle partie puis appuyer sur le bouton "New Game".

Après le premier lancement, l'écran d'accueil permet de charger une partie enregistrée grâce au bouton "Load Game". Le bouton "Delete" permet de supprimer une sauvegarde.

![](assets/load_saved.png)

### Les menus

Le menu "Settings" permet d'afficher/cacher la grille de la carte (également disponible avec le raccourci Ctrl+L) :

<img src="assets/map_with_ressources.png" alt="map" width="300"/>

<img src="assets/map_with_grid.png" alt="map" width="300"/>

Le menu "Game" permet de revenir à l'écran d'accueil ou de sauvegarder la partie en cours.

![](assets/menu_game.png)

### Déplacer son personnage sur la carte

Pour déplacer son personnage, il faut utiliser les touches ZQSD de son clavier:

- Z : haut.
- Q : gauche.
- S : bas.
- D : droite.

### Récolter des ressources

Le joueur récolte les ressources en appuyant sur la touche **F** de son clavier. La ressource récoltée et sa quantité dépend du terrain sur lequel le joueur est présent et des terrains alentours.

Les ressources récoltables sur un case ainsi que leur nombre restant sont affiché lorsque le joueur passe la souris sur la case

![ressources hover](assets/ressource_hover.png)

Les ressources sont affichées à droite de la carte jeu.

![carte_avec_ressources](assets/map_with_ressources.png)

### Construire un bâtiment

Le joueur doit utiliser l’interface en bas de la carte pour choisir le bâtiment qu’il désire construire. Pour le construire, le joueur doit :

- avoir récolté assez de ressources par rapport à la recette du bâtiment,
- se placer en face d'une case constructible,
- appuyer sur la touche C.

![](assets/map_choose_building.png)

### Afficher les informations d'une case

Un clique de la sourie sur une case la met en surbrillance et affiche les informations qui lui sont relatives : type de terrain, batiment présent, _crafts disponibles (à venir)_.

![](assets/map_selected_forest.png)

![](assets/map_selected_building.png)