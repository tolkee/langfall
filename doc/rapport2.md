<div align="center">
    <img alt="langfall" src="assets/logo.png" width="80" />
    <h1>
  Rapport de projet
</h1>
</div>

**L'équipe :** Marine Blasius, Rémi Barra, Alexandre Condamine, Valentin Hou et Guillaume Lacoste

> Langfall est un jeu 2D dans lequel le joueur pourra récolter des ressources, créer des batiments avec celles-ci, intérargir avec ces batiments pour fabriquer des objets, effectuer des quêtes...

## 1. User stories

Voici la liste de nos user stories et leur répartition par versions (par itérations) :

- **Langfall v1 :**
  - Le joueur peut voit la carte.
  - Le joueur peut déplacer son personnage sur la carte.
  - Le joueur peut récolter des ressources sur les différents terrains de la carte.
  - _(Peut-être en V2) Le joueur peut afficher les informations d'une case de la carte._
- **Langfall v2 :**
  - _(suite) Le joueur peut afficher les informations d'une case de la carte._
  - Le joueur peut construire un bâtiment sur la carte.
  - Le joueur peut détruire un bâtiment sur la carte.
  - Le joueur peut déplacer un bâtiment sur la carte.
  - Le joueur peut sauvegarder sa partie en cours.
  - _(suite en v3)Le joueur peut utiliser les batiments présents sur la carte._
- **Langfall v3 :**
  - Les ressources d'une case se régénèrent
  - Le joueur peut déplacer améliorer ses batiments/équipements.
  - Le joueur peut construire un pont.
  - Le joueur peut jouer au jeu de façon scénarisé.
  - L'utilisateur peut créer des templates de cartes à partir d'un éditeur.

<div style="page-break-after: always;"></div>

## 2. Conception

![model](assets/dc/dc-model.png)
![building](assets/dc/dc-building.png)
![elements](assets/dc/dc-elements.png)
![land](assets/dc/dc-land.png)
![map](assets/dc/dc-map.png)
![utils](assets/dc/dc-utils.png)

## 3. Choix techniques

- Gestion de versions: **Git**
- Langage : **Java 11**
  - Laibrairie graphique : **JavaFX**
  - Librairie JSON : **GSON**
- Outil de build : **Gradle** 
  - Utilise les plugins :
    - Application : ajoute une tâche Gradle permettant de lancer le programme en utilisant la configuration Gradle,
    - Java,
    - JavaFX : fournit les dépendences JavaFX nécessaire.

## 4. Gestion de projet

L'équipe a choisi de fonctionner avec la méthode SCRUM.

Nous utilisons l'outil de gestion de projet inclus dans Gitlab.
