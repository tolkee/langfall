# Rapport individuel d'activité

de Marine Blasius

## Première itération

| Tâche                                                        | Issue                                                 | Degré d'avancement | Commentaire                                                  |
| ------------------------------------------------------------ | ----------------------------------------------------- | ------------------ | ------------------------------------------------------------ |
| Définition du cahier des charges et des user stories         | [#6](https://gitlab.com/Ato0m3/langfall/-/issues/6)   | Terminé.           | Fait avec toute l'équipe.                                    |
| Conception de l'architecture du projet                       | [#14](https://gitlab.com/Ato0m3/langfall/-/issues/14) | Terminé.           | Fait avec toute l'équipe.                                    |
| Apprendre à utiliser JavaFX                                  | [#20](https://gitlab.com/Ato0m3/langfall/-/issues/20) | Terminé.           | La branche ExemplesJavaFX contient des exemples sur la plupart des composants JavaFX. |
| Le joueur peut récolter des ressources sur les différents terrains de la carte | [#25](https://gitlab.com/Ato0m3/langfall/-/issues/25) | Terminé.           |                                                              |

