<div align="center">
    <img alt="langfall" src="doc/assets/logo.png" width="80" />
    <h1>
  Langfall v2
</h1>
<h3>
  Survival/Sandbox 2D Game
  </h3>
<p>
L'équipe : Marine Blasius, Rémi Barra, Alexandre Condamine, Valentin Hou et Guillaume Lacoste
</p>
</div>

> [Manuel utilisateur](doc/manuel-utilisateur1.md)  
> [Rapport projet](doc/rapport1.md)

## Description du jeu

Notre objectif est de réaliser un jeu de gestion en vue de dessus.
Dans celui-ci, un personnage évolue sur un terrain découpé en cases.
Selon la case sur laquelle il se trouve, le personnage peut effectuer diverses actions qui varient selon la nature de la case :

- Plaine : Le personnage peut se déplacer sur la case et y construire un bâtiment en fonction des ressources qu'il possède.
- Forêt : Le personnage ne peut pas se déplacer sur la case mais peut récolter du bois.
- Montagne : Le personnage ne peut pas se déplacer sur la case mais peut récolter de la roche.
- Eau : Le personnage ne peut pas se déplacer sur la case mais peut récolter de l'eau.

Le GUI se compose d’un affichage des ressources du joueur et possèdera à l'avenir un menu contextuel où il sera possible de choisir une action : collecter des ressources ou construire un bâtiment selon la liste de bâtiments constructibles.
Il y sera également affiché des informations liées à la case sélectionnée (type de terrain, bâtiment présent sur la case, etc.)

## Images

![map](doc/assets/map_with_ressources.png)
![map_with_grid](doc/assets/map_with_grid.png)
