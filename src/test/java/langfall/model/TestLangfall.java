package langfall.model;

import langfall.model.elements.Resources;
import langfall.model.land.Land;
import langfall.model.map.Map;
import langfall.model.utils.Direction;
import langfall.model.utils.Position;
import org.junit.Before;

/**
 * Test template for Langfall
 */

public class TestLangfall {

    Player player;
    Game game;
    final int QUANTITY_RESOURCES = 50;
    final int NB_RESOURCES = 3;

    @Before

    public void setUp() throws Exception {
        this.game = new Game("GameTest", "map1.json");

        this.player = this.game.getPlayer();
        this.player.setDirection(Direction.SOUTH);
        // Adds resources to the player
        this.player.addElement(Resources.ORE, QUANTITY_RESOURCES);
        this.player.addElement(Resources.WOOD, QUANTITY_RESOURCES);
        this.player.addElement(Resources.WATER, QUANTITY_RESOURCES);
    }

    /**
     * Set the cells around the player with a given land.
     *
     * @param land land to set
     */
    void setCellsAround(Land land) {
        Map map = this.game.getMap();
        Position playerPos = this.player.getPosition();

        map.getCell(new Position(playerPos.getX(), playerPos.getY() + 1)).setLand(land);
        map.getCell(new Position(playerPos.getX(), playerPos.getY() - 1)).setLand(land);
        map.getCell(new Position(playerPos.getX() + 1, playerPos.getY())).setLand(land);
        map.getCell(new Position(playerPos.getX() - 1, playerPos.getY())).setLand(land);
    }

}
