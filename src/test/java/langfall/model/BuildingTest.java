package langfall.model;

import langfall.exceptions.EmptyCellException;
import langfall.exceptions.OccupiedCellException;
import langfall.model.building.Building;
import langfall.model.building.Forge;
import langfall.model.building.Sawmill;
import langfall.model.land.Forest;
import langfall.model.land.Grass;
import langfall.model.land.River;
import langfall.model.map.Cell;
import langfall.model.utils.Position;
import org.junit.Assert;
import org.junit.Test;

public class BuildingTest extends TestLangfall {

    @Test
    public void shouldBuild() {
        setCellsAround(new Grass());
        this.player.build(Sawmill.class);
        Building building = getCellInFrontOfPlayer().getBuilding();
        Assert.assertNotNull(building);
        Assert.assertEquals("Sawmill", building.getName());
    }

    @Test
    public void shouldNotBuildOnFarmableLand() {
        setCellsAround(new Forest());
        this.player.build(Sawmill.class);
        Building building = getCellInFrontOfPlayer().getBuilding();
        Assert.assertNull(building);
    }

    @Test
    public void shouldNotBuildOverBuilding() {
        setCellsAround(new Grass());
        this.player.build(Sawmill.class);
        this.player.build(Forge.class);
        Building building = getCellInFrontOfPlayer().getBuilding();
        Assert.assertEquals("Sawmill", building.getName());
        Assert.assertNotEquals("Forge", building.getName());
    }

    private Cell getCellInFrontOfPlayer() {
        Position cellPos = this.player.getPosition().clone();
        cellPos.add(0, 1); // Player direction is set to SOUTH
        return this.game.getMap().getCell(cellPos);
    }

    private Cell getCellBehindThePlayer() {
        Position cellPos = this.player.getPosition().clone();
        cellPos.add(0, -1); // Player direction is set to SOUTH
        return this.game.getMap().getCell(cellPos);
    }

    @Test
    public void shouldDestroy() {
        setCellsAround(new Grass());
        this.player.build(Sawmill.class);
        Building building = getCellInFrontOfPlayer().getBuilding();
        try {
            player.destroyBuilding(getCellInFrontOfPlayer());
        } catch (EmptyCellException e) {
            Assert.fail("La destruction de " + building.getName() + " a echoue");
        }
        Assert.assertNull(getCellInFrontOfPlayer().getBuilding());
    }

    @Test
    public void shouldNotDestroyOnEmptyCases() {
        setCellsAround(new Grass());
        try {
            player.destroyBuilding(getCellInFrontOfPlayer());
            Assert.fail("La destruction sur une case vide devrait echouer");
        } catch (EmptyCellException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void shouldMove() {
        setCellsAround(new Grass());
        this.player.build(Sawmill.class);
        Building building = getCellInFrontOfPlayer().getBuilding();
        try {
            player.startMovingBuilding(getCellInFrontOfPlayer());
            player.moveBuilding(getCellBehindThePlayer());
        } catch (EmptyCellException | OccupiedCellException e) {
            Assert.fail("Le deplacement de " + building.getName() + " a echoue");
        }
        Assert.assertNotNull(getCellBehindThePlayer().getBuilding());
        Assert.assertNull(getCellInFrontOfPlayer().getBuilding());
    }

    @Test
    public void shouldNotMove() {
        setCellsAround(new Grass());
        this.player.build(Sawmill.class);
        Building building = getCellInFrontOfPlayer().getBuilding();
        try {
            player.startMovingBuilding(getCellBehindThePlayer());
        } catch (EmptyCellException e) {
            Assert.assertTrue(true);
        }
        try {
            player.startMovingBuilding(getCellInFrontOfPlayer());
        } catch (EmptyCellException e) {
            Assert.fail("La cellule ne devrait pas etre vide");
        }
        try {
            player.moveBuilding(getCellInFrontOfPlayer());
        } catch (OccupiedCellException e1) {
            Assert.assertNotNull(getCellInFrontOfPlayer().getBuilding());
            Assert.assertNull(getCellBehindThePlayer().getBuilding());
            try {
                River river = new River();
                getCellBehindThePlayer().setLand(river);
                player.moveBuilding(getCellBehindThePlayer());
            } catch (OccupiedCellException e2) {
                Assert.assertNotNull(getCellInFrontOfPlayer().getBuilding());
                Assert.assertNull(getCellBehindThePlayer().getBuilding());
            }
        }
    }
}
