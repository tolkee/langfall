package langfall.model;

import langfall.model.elements.Resources;
import langfall.model.land.Grass;
import langfall.model.land.River;
import langfall.model.map.Map;
import langfall.model.utils.Direction;
import langfall.model.utils.Position;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for the class Player.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class PlayerTest extends TestLangfall {

    @Test
    public void shouldNotMoveOutsideMap() {
        for (Direction dir : Direction.values()) {
            Position initialPos;
            if (dir == Direction.NORTH || dir == Direction.WEST) {
                initialPos = new Position(0, 0);
            } else {
                initialPos = new Position(Map.WIDTH - 1, Map.HEIGHT - 1);
            }
            this.player.setPosition(initialPos);
            this.player.move(dir);
            Assert.assertEquals(initialPos, this.player.getPosition());
            Assert.assertEquals(dir, this.player.getDirection());
        }
    }

    @Test
    public void shouldNotMoveOnNotPassableLand() {
        Position initialPos = new Position(2, 2);
        this.player.setPosition(initialPos);

        setCellsAround(new River());

        for (Direction dir : Direction.values()) {
            this.player.move(dir);
            Assert.assertEquals(initialPos, this.player.getPosition());
            Assert.assertEquals(dir, this.player.getDirection());
        }
    }

    @Test
    public void shouldMove() {
        Position initialPos = new Position(6, 6);
        this.player.setPosition(initialPos);

        setCellsAround(new Grass());

        Position expectedPos = null;

        for (Direction dir : Direction.values()) {
            this.player.setPosition(initialPos);
            this.player.move(dir);

            switch (dir) {
                case NORTH:
                    expectedPos = new Position(initialPos.getX(), initialPos.getY() - 1);
                    break;
                case SOUTH:
                    expectedPos = new Position(initialPos.getX(), initialPos.getY() + 1);
                    break;
                case WEST:
                    expectedPos = new Position(initialPos.getX() - 1, initialPos.getY());
                    break;
                case EAST:
                    expectedPos = new Position(initialPos.getX() + 1, initialPos.getY());
                    break;

            }
            Assert.assertEquals(expectedPos, this.player.getPosition());
            Assert.assertEquals(dir, this.player.getDirection());
        }
    }

    @Test
    public void shouldFarmLand() {
        Position initialPos = new Position(2, 2);
        this.player.setPosition(initialPos);

        River river = new River();
        setCellsAround(river);

        System.out.println(this.player.getInventory().get(Resources.WATER));
        player.farm();
        player.farm();
        player.farm();

        int nbWater = this.player.getInventory().get(Resources.WATER);

        Assert.assertEquals(this.QUANTITY_RESOURCES + 3, nbWater);
        Assert.assertEquals(this.NB_RESOURCES, this.player.getInventory().size());
    }

    @Test
    public void shouldNotFarmLand() {
        Position initialPos = new Position(2, 2);
        this.player.setPosition(initialPos);

        setCellsAround(new Grass());

        player.farm();
        player.farm();
        player.farm();

        Assert.assertEquals(this.NB_RESOURCES, this.player.getInventory().size());
    }
}