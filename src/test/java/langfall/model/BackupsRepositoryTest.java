package langfall.model;

import java.io.IOException;
import java.util.List;
import langfall.exceptions.InvalidTemplateException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * Tests for the class BackupsRepository.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class BackupsRepositoryTest extends TestLangfall {

    private int initialRepoSize;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.initialRepoSize = BackupsRepository.getAllBackupsName().size();
    }

    @After
    public void tearDown() throws Exception {
        BackupsRepository.deleteBackup(this.game.getTitle());
    }

    @Test
    public void shouldGetAllBackupsSaved() throws InvalidTemplateException, IOException {
        Game game1 = new Game("GameTest1", "map1.json");
        Game game2 = new Game("GameTest2", "map1.json");

        BackupsRepository.saveBackup(this.game);
        BackupsRepository.saveBackup(game1);
        BackupsRepository.saveBackup(game2);

        List<String> backups = BackupsRepository.getAllBackupsName();
        Assert.assertEquals(this.initialRepoSize + 3, backups.size());
        Assert.assertTrue(backups.contains(this.game.getTitle()));
        Assert.assertTrue(backups.contains(game1.getTitle()));
        Assert.assertTrue(backups.contains(game2.getTitle()));

        BackupsRepository.deleteBackup(game1.getTitle());
        BackupsRepository.deleteBackup(game2.getTitle());
    }

    @Test
    public void shouldSave() throws IOException {
        BackupsRepository.saveBackup(this.game);
        List<String> backups = BackupsRepository.getAllBackupsName();

        Assert.assertEquals(this.initialRepoSize + 1, backups.size());
        Assert.assertTrue(backups.contains(this.game.getTitle()));

        this.game.getPlayer().setName("EAS");

        BackupsRepository.saveBackup(this.game);

        Game backup = BackupsRepository.loadBackup(this.game.getTitle());

        Assert.assertEquals(this.game.getPlayer().getName(), backup.getPlayer().getName());
    }

    @Test
    public void shouldDelete() throws IOException {
        BackupsRepository.saveBackup(this.game);

        BackupsRepository.deleteBackup(this.game.getTitle());
        List<String> backups = BackupsRepository.getAllBackupsName();

        Assert.assertEquals(this.initialRepoSize, backups.size());
        Assert.assertFalse(backups.contains(this.game.getTitle()));
    }

    @Test
    public void shouldLoad() throws IOException {
        BackupsRepository.saveBackup(this.game);

        Game backup = BackupsRepository.loadBackup(this.game.getTitle());

        Assert.assertNotNull(backup);
        Assert.assertEquals(this.game.getTitle(), backup.getTitle());
    }

    @Test(expected = IOException.class)
    public void shouldNotLoad() throws IOException {
        Game backup = BackupsRepository.loadBackup(this.game.getTitle());
    }
}