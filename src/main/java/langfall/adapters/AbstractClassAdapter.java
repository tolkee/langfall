package langfall.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

/**
 * Class representing the adapter indicating to GSON  how to serialize and deserialize an abstract
 * class.
 * <p>
 * Explication : GSON cannot deserialize an abstract class because an abstract class can't be
 * instanced. So we tell GSON to add a field (CLASS_META_KEY) when he is serializing a class
 * (extending an abstract class), which contains the concrete class. Then, when GSON will
 * deserialize, he will use this field to instance the concrete class.
 * <p>
 * Example : for Grass which extends Land : (without the adapter)
 * <p>
 * "land": { "name": "Grass", "passable": true, "buildable": [ "langfall.model.building.Sawmill",
 * "langfall.model.building.Forge" ]}
 * <p>
 * <p>
 * * Example : for Grass which extends Land : (with the AbstractClassAdapter)
 * <p>
 * "land": { "name": "Grass", "passable": true, "buildable": [ "langfall.model.building.Sawmill",
 * "langfall.model.building.Forge" ], "CLASS_META_KEY": "langfall.model.land.Grass" }
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class AbstractClassAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {

    /**
     * The fields's key containing the concrete class
     */
    public static final String CLASS_META_KEY = "CLASS_META_KEY";

    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        JsonElement jsonElement = context.serialize(src, src.getClass());
        jsonElement.getAsJsonObject()
            .addProperty(CLASS_META_KEY, src.getClass().getCanonicalName());
        return jsonElement;
    }

    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String className = jsonObject.get(CLASS_META_KEY).getAsString();
        try {
            return context.deserialize(json, Class.forName(className));
        } catch (ClassNotFoundException e) {
            throw new JsonParseException("Invalid " + CLASS_META_KEY, e);
        }
    }
}
