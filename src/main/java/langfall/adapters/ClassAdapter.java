package langfall.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

/**
 * Class representing the adapter indicating to GSON  how to serialize and deserialize a Class
 * type.
 * <p>
 * Explication : GSON basically serialize a type Class (ex: Grass.class) to "class nameoftheclass".
 * But doesn't know how to deserialize it. So the ClassAdapter say GSON to serialize a type Class
 * with theclass.getName() and to deserialize with Class.forName("nameoftheclass");
 * <p>
 * Example : resource of the player (without the adapter)
 * <p>
 * "class langfall.model.resource.Wood": 10,
 * <p>
 * <p>
 * * Example : resource of the player (with the ClassAdapter)
 * <p>
 * "langfall.model.resource.Wood": 10,
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */
public class ClassAdapter implements JsonDeserializer<Class>, JsonSerializer<Class> {

    @Override
    public JsonElement serialize(Class src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getName());
    }

    @Override
    public Class deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        try {
            return Class.forName(json.getAsString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }
}
