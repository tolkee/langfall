package langfall.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import langfall.model.elements.Element;

/**
 * An GSON adapter for elements.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */
public class ElementAdapter implements JsonDeserializer<Element> {

    @Override
    public Element deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        String[] res = ElementSubTypeAdapter.splitJson(json);
        Class klass;
        try {
            klass = Class.forName(res[1]);
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e.getMessage());
        }
        return context.deserialize(json, klass);
    }
}
