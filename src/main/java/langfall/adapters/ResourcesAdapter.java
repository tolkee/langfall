package langfall.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import langfall.model.elements.Resources;

/**
 * An GSON adapter for resources.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */
public class ResourcesAdapter extends ElementSubTypeAdapter<Resources> implements
    JsonDeserializer<Resources> {

    public ResourcesAdapter() {
        super(Resources.class);
    }

    @Override
    public Resources deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        String[] res = splitJson(json);

        if (res[0].equals(ElementSubTypeAdapter.ENUM_KEY) && res[1].equals(klass.getName())) {
            return Resources.valueOf(res[2]);
        }

        throw new JsonParseException("");
    }
}
