package langfall.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import langfall.model.elements.CraftableElements;

/**
 * An GSON adapter for craftable elements.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */
public class CraftableElementsAdapter extends ElementSubTypeAdapter<CraftableElements> implements
    JsonDeserializer<CraftableElements> {

    public CraftableElementsAdapter() {
        super(CraftableElements.class);
    }

    @Override
    public CraftableElements deserialize(JsonElement json, Type typeOfT,
        JsonDeserializationContext context)
        throws JsonParseException {

        String[] res = splitJson(json);

        if (res[0].equals(ElementSubTypeAdapter.ENUM_KEY) && res[1].equals(klass.getName())) {
            return CraftableElements.valueOf(res[2]);
        }

        throw new JsonParseException("");
    }
}
