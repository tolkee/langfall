package langfall.adapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import langfall.model.elements.Element;

/**
 * An GSON adapter for element subtypes.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */
public abstract class ElementSubTypeAdapter<T extends Element> implements JsonSerializer<T> {

    public static final String ENUM_KEY = "ENUM";
    public static final String CLASS_KEY = "CLASS";

    /**
     * The subtype to adapt for GSON.
     */
    protected final Class<? extends Element> klass;

    /**
     * The class type of the subtype: enum or class.
     */
    private final String klassType;

    public ElementSubTypeAdapter(Class<? extends Element> klass) {
        this.klass = klass;
        if (klass.isEnum()) {
            this.klassType = ENUM_KEY;
        } else {
            this.klassType = CLASS_KEY;
        }
    }

    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(klassType + " " + klass.getName() + " " + src);
    }

    /**
     * Split a json element on white spaces.
     *
     * @param json the element to split
     * @return the splited element in an array
     * @throws JsonParseException if the splited element's array doesn't have a length of 3.
     */
    public static String[] splitJson(JsonElement json) throws JsonParseException {
        String[] res = json.getAsString().split(" ");
        if (res.length != 3) {
            throw new JsonParseException("Wrong element format.");
        }
        return res;
    }
}
