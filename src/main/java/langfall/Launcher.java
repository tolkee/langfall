package langfall;

import langfall.view.Langfall;

/**
 * Class used to launch the Application JavaFX (useful for the jar)
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */


public class Launcher {

    /* ----------- !!! DON'T MODIFY THIS CLASS !!! ----------- */

    public static void main(String[] args) {
        Langfall.main(args);
    }
}
