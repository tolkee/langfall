package langfall;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import langfall.adapters.AbstractClassAdapter;
import langfall.adapters.ClassAdapter;
import langfall.adapters.CraftableElementsAdapter;
import langfall.adapters.ElementAdapter;
import langfall.adapters.ResourcesAdapter;
import langfall.model.building.Building;
import langfall.model.elements.CraftableElements;
import langfall.model.elements.Element;
import langfall.model.elements.Resources;
import langfall.model.land.Land;

/**
 * Class containing configurated instance of librairy tools.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */


public class Config {

    /**
     * The instance of GSON librairy (librairy for JSON)
     * <p>
     * Config: [prettier: true]
     * <p>
     * See : <a href="https://github.com/google/gson">GSON github</a>
     */
    public static final Gson GSON =
        new GsonBuilder()
            .enableComplexMapKeySerialization()
            .excludeFieldsWithoutExposeAnnotation()
            .setPrettyPrinting()
            .registerTypeAdapter(Element.class, new ElementAdapter())
            .registerTypeAdapter(Resources.class, new ResourcesAdapter())
            .registerTypeHierarchyAdapter(CraftableElements.class, new CraftableElementsAdapter())
            .registerTypeAdapter(Class.class, new ClassAdapter())
            .registerTypeAdapter(Land.class, new AbstractClassAdapter<>())
            .registerTypeAdapter(Building.class, new AbstractClassAdapter<>())
            .create();
}
