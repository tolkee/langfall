package langfall.exceptions;

/**
 * Exception indicating of an invalid template (bad width/height, bad ids etc...).
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class InvalidTemplateException extends Exception {

    public InvalidTemplateException(String msg) {
        super(msg);
    }

    public InvalidTemplateException(String msg, Throwable err) {
        super(msg, err);
    }
}
