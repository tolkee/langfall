package langfall.exceptions;

/**
 * Exception indicating that a cell is empty.
 *
 * @author Hou Valentin
 * @version 1.0
 */

public class EmptyCellException extends Exception {

    public EmptyCellException(String msg) {
        super(msg);
    }

    public EmptyCellException(String msg, Throwable err) {
        super(msg, err);
    }
}
