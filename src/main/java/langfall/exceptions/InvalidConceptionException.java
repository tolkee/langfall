package langfall.exceptions;

/**
 * Exception indicating a wrong conception in the project (for example a building doesn't have an
 * empty constructor).
 *
 * @author Marine Blasius
 * @version 1.0
 */
public class InvalidConceptionException extends RuntimeException {

    public InvalidConceptionException(String msg) {
        super(msg);
    }

    public InvalidConceptionException(String msg, Throwable err) {
        super(msg, err);
    }
}
