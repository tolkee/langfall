package langfall.exceptions;

/**
 * Exception indicating that there isn't enough resources.
 *
 * @author Blasius Marine
 * @version 1.0
 */

public class InsufficientResourcesException extends Exception {

    public InsufficientResourcesException(String msg) {
        super(msg);
    }

    public InsufficientResourcesException(String msg, Throwable err) {
        super(msg, err);
    }
}
