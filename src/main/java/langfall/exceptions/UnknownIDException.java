package langfall.exceptions;

/**
 * Exception indicating of an unkown ID (if not in LandID, BuildingID etc..)
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class UnknownIDException extends Exception {

    public UnknownIDException(String msg) {
        super(msg);
    }

    public UnknownIDException(String msg, Throwable err) {
        super(msg, err);
    }
}
