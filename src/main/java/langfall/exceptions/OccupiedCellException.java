package langfall.exceptions;

/**
 * Exception indicating that a cell is occupied.
 *
 * @author Hou Valentin
 * @version 1.0
 */

public class OccupiedCellException extends Exception {

    public OccupiedCellException(String msg) {
        super(msg);
    }

    public OccupiedCellException(String msg, Throwable err) {
        super(msg, err);
    }
}
