package langfall.tasks;


import java.util.Timer;
import java.util.TimerTask;

/**
 * Abstract class representing a background game task.
 *
 * @author Guillaume Lacoste
 * @version 3.0
 */

public abstract class BackgroundTask extends TimerTask {

    /**
     * The task timer
     */
    protected Timer timer;

    /**
     * Start the task
     */
    public abstract void start();

    /**
     * Stop the task
     */
    public void stop() {
        this.timer.cancel();
        this.timer.purge();
    }
}
