package langfall.tasks;

import java.util.Timer;
import langfall.events.LandModifiedEvent;
import langfall.model.Game;
import langfall.model.land.FarmableLand;
import langfall.model.map.Cell;
import langfall.model.map.Map;

/**
 * Class representing the background game task which regenerate resources.
 *
 * @author Guillaume Lacoste
 * @version 3.0
 */

public class RegenerateResourcesTask extends BackgroundTask {

    private Game game;

    public RegenerateResourcesTask(Game game) {
        this.game = game;
    }

    @Override
    public void start() {
        this.timer = new Timer();

        // 2 minutes delay and period
        this.timer.scheduleAtFixedRate(this, 20000, 20000);
    }

    @Override
    public void run() {
        for (int y = 0; y < Map.HEIGHT; y++) {
            for (int x = 0; x < Map.WIDTH; x++) {
                Cell cell = game.getMap().getCell(x, y);
                if (cell.getLand() instanceof FarmableLand) {
                    if (cell.getBuilding() == null && !game.getPlayer().getPosition()
                        .equals(cell.getPos())) {
                        ((FarmableLand) cell.getLand()).regenerate();
                        this.game.dispatchEvent(new LandModifiedEvent(cell.getPos()));
                    }
                }
            }
        }
    }
}
