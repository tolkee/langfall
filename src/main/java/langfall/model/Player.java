package langfall.model;

import com.google.gson.annotations.Expose;
import java.util.Collections;
import java.util.HashMap;
import langfall.events.BuildingModifiedEvent;
import langfall.events.InventoryEvent;
import langfall.events.LandModifiedEvent;
import langfall.events.PlayerMovedEvent;
import langfall.exceptions.EmptyCellException;
import langfall.exceptions.InsufficientResourcesException;
import langfall.exceptions.OccupiedCellException;
import langfall.model.building.Building;
import langfall.model.elements.Element;
import langfall.model.land.FarmableLand;
import langfall.model.map.Cell;
import langfall.model.utils.Direction;
import langfall.model.utils.Position;

/**
 * Class representing a player in Langfall.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class Player {

    /**
     * The name of the player.
     */
    @Expose
    private String name;

    /**
     * The position of the player.
     */
    @Expose
    private Position position;

    /**
     * The direction of the player.
     */
    @Expose
    private Direction direction;

    /**
     * The player resources.
     */
    @Expose
    private final java.util.Map<Element, Integer> inventory;

    /**
     * The game containing the player.
     */
    private Game game;

    /**
     * The status possible for the player.
     */
    public enum Status {idle, movingABuilding}

    /**
     * The current status of the player.
     */
    private Status state;

    /**
     * The cell selected by the player.
     */
    private Cell selectedCell;

    /**
     * Constructor: initializes a player with given name, position and game.
     *
     * @param name     The name of the player
     * @param position The position of the player
     * @param game     The game where the player is
     */
    public Player(String name, Position position, Game game) {
        this.name = name;
        this.position = position;
        this.direction = Direction.SOUTH;
        this.inventory = new HashMap<>();
        this.game = game;
        this.state = Status.idle;
        this.selectedCell = null;
    }

    /**
     * Returns the game.
     *
     * @return the game of the player
     */
    public Game getGame() {
        return game;
    }

    /**
     * Sets the game of the player.
     *
     * @param game the game
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * Return the name of the player.
     *
     * @return The name of the player.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the player.
     *
     * @param name The new name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the select cell.
     *
     * @return the selected cell
     */
    public Cell getSelectedCell() {
        return this.selectedCell;
    }

    /**
     * Return the position of the player.
     *
     * @return the position of the player
     */
    public Position getPosition() {
        return position.clone();
    }

    /**
     * Set the position of the player.
     *
     * @param position The new position.
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * Return the direction of the player.
     *
     * @return The direction of the player.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Set the direction of the player.
     *
     * @param direction The new direction.
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Returns the current state of the player
     *
     * @return The current state of the player
     */
    public Status getState() {
        return this.state;
    }

    /**
     * Returns an unmodifiable map of the player's inventory.
     *
     * @return the inventory
     */
    public java.util.Map<Element, Integer> getInventory() {
        return Collections.unmodifiableMap(inventory);
    }

    /**
     * Moves the player to a given direction.
     *
     * @param direction the direction
     */
    public void move(Direction direction) {
        Position from = this.position.clone();
        this.direction = direction;
        Position newPos = getFrontPosition();

        if (isMoveValid(newPos)) {
            this.position = newPos;
        }

        game.dispatchEvent(new PlayerMovedEvent(from, this.position));
    }

    /**
     * Farms the land in front of the player.
     */
    public void farm() {
        Position landPosition = getFrontPosition();
        Cell cell = this.game.getMap().getCell(landPosition);

        if (cell != null && cell.getLand() instanceof FarmableLand) {
            FarmableLand land = (FarmableLand) cell.getLand();
            try {
                addElement(land.farm(), 1);
                this.game.dispatchEvent(new LandModifiedEvent(landPosition));
                this.game.dispatchEvent(new InventoryEvent());
            } catch (InsufficientResourcesException e) {
                // no more resources on this land: nothing to do
            }
        }
    }

    /**
     * Builds the building in front of the player.
     *
     * @param buildingClass the class of the building to build
     */
    public void build(Class<? extends Building> buildingClass) {
        Position buildingPosition = getFrontPosition();
        Cell buildingCell = this.game.getMap().getCell(buildingPosition);
        if (buildingCell != null) {
            Building building = Building.getBuildingFromCLass(buildingClass);
            java.util.Map<Element, Integer> recipe = building.getRecipe();
            if (buildingCell.getLand().isBuildable(building.getClass())
                && hasEnoughElements(recipe) && buildingCell.getBuilding() == null) {
                recipe.forEach(this::removeElement);
                buildingCell.setBuilding(building);
                this.game.dispatchEvent(new BuildingModifiedEvent(buildingPosition));
            }
        }
    }

    /**
     * Destroy the building on the selected cell, provided it exists.
     *
     * @param cell the cell from which you want to destroy the building
     * @throws EmptyCellException if there are no building on the selected cell.
     */
    public void destroyBuilding(Cell cell) throws EmptyCellException {
        Building toDestroy = cell.getBuilding();
        if (toDestroy == null) {
            throw new EmptyCellException("No building on cell " + cell.getPos().toString());
        }
        java.util.Map<Element, Integer> recipe = toDestroy.getRecipe();
        recipe.forEach(this::addElement);

        cell.setBuilding(null);
        this.game.dispatchEvent(new BuildingModifiedEvent(cell.getPos()));
    }

    /**
     * Selects the source cell from which the building should be moved and pass the player's status
     * to movingABuilding.
     *
     * @param sourceCell the cell from whom you want to move the building
     * @throws EmptyCellException if no building on the selected cell.
     */
    public void startMovingBuilding(Cell sourceCell) throws EmptyCellException {
        if (sourceCell.getBuilding() == null) {
            throw new EmptyCellException(
                "No building on cell " + sourceCell.getPos().toString());
        }
        this.selectedCell = sourceCell;
        this.state = Status.movingABuilding;
    }

    /**
     * Move the building from the selectedCell to the targetCell.
     *
     * @param targetCell the cell from which you want to move the building
     * @throws OccupiedCellException if the targeted cell is not buildable or already has a
     *                               building.
     */
    public void moveBuilding(Cell targetCell) throws OccupiedCellException {
        Building toMove = selectedCell.getBuilding();
        if (!(targetCell.getLand().isBuildable(toMove.getClass())
            && targetCell.getBuilding() == null)) {
            throw new OccupiedCellException(
                "Cell " + targetCell.getPos().toString() + " is occupied.");
        }
        // else
        selectedCell.setBuilding(null);
        targetCell.setBuilding(toMove);
        this.state = Status.idle;
        this.game.dispatchEvent(new BuildingModifiedEvent(selectedCell.getPos()));
        this.game.dispatchEvent(new BuildingModifiedEvent(targetCell.getPos()));
        this.setIdle();
    }

    /**
     * Makes the player idle.
     */
    public void setIdle() {
        this.selectedCell = null;
        this.state = Status.idle;
    }

    /**
     * Validates the position for a player move. The new position is valid if the new cell is
     * passable or has a building.
     *
     * @param pos The position to validate.
     * @return true if pos is valid, false otherwise
     */
    private boolean isMoveValid(Position pos) {
        Cell cell = game.getMap().getCell(pos);
        return cell != null && (cell.getLand().isPassable() || cell.getBuilding() != null);
    }

    /**
     * Returns the position in front of the player.
     *
     * @return the position in front of the player
     */
    public Position getFrontPosition() {
        Position newPos = position.clone();
        switch (direction) {
            case SOUTH:
                newPos.add(0, 1);
                break;
            case NORTH:
                newPos.subtract(0, 1);
                break;
            case WEST:
                newPos.subtract(1, 0);
                break;
            case EAST:
                newPos.add(1, 0);
                break;

        }
        return newPos;
    }

    /**
     * Adds quantity elements to the player inventory.
     *
     * @param element  the class of the resource to remove
     * @param quantity the quantity to remove
     */
    public void addElement(Element element, int quantity) {
        if (!inventory.containsKey(element)) {
            inventory.put(element, quantity);
        } else {
            inventory.replace(element, inventory.get(element) + quantity);
        }
    }

    /**
     * Removes quantity elements from the player inventory or does nothing if there isn't enough
     * elements to remove. If the quantity left is 0, removes the element from the inventory.
     *
     * @param element  the class of the resource to remove
     * @param quantity the quantity to remove
     */
    public void removeElement(Element element, int quantity) {
        if (hasEnoughElements(element, quantity)) {
            this.inventory.replace(element, inventory.get(element) - quantity);
            if (this.inventory.get(element) == 0) {
                this.inventory.remove(element);
            }
        }
    }

    /**
     * Removes the recipe from the player inventory.
     *
     * @param recipe the recipe to remove
     * @return true if hasEnoughElements, false otherwise
     */
    public boolean removeElement(java.util.Map<Element, Integer> recipe) {
        for (java.util.Map.Entry<Element, Integer> ingredient : recipe
            .entrySet()) {
            if (hasEnoughElements(ingredient.getKey(), ingredient.getValue())) {
                removeElement(ingredient.getKey(), ingredient.getValue());
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks that the player has at least quantity of the element.
     *
     * @param element  the resource to check
     * @param quantity the quantity to check
     * @return true of there is enough elements, false otherwise
     */
    public boolean hasEnoughElements(Element element, int quantity) {
        if (!inventory.containsKey(element)
            || quantity > inventory.get(element)) {
            return false;
        }
        return true;
    }

    /**
     * Checks that the player has enough elements against the recipe.
     *
     * @param recipe the recipe to check
     * @return true of there is enough elements, false otherwise
     */
    public boolean hasEnoughElements(java.util.Map<Element, Integer> recipe) {
        for (java.util.Map.Entry<Element, Integer> ingredient : recipe
            .entrySet()) {
            if (!hasEnoughElements(ingredient.getKey(), ingredient.getValue())) {
                return false;
            }
        }
        return true;
    }
}
