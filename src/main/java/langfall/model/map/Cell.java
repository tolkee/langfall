package langfall.model.map;

import com.google.gson.annotations.Expose;
import langfall.model.building.Building;
import langfall.model.land.Land;
import langfall.model.utils.Position;

/**
 * Class representing a cell from a map.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class Cell {

    /**
     * The cell's position.
     */
    @Expose
    private final Position pos;

    /**
     * The cell's land.
     */
    @Expose
    private Land land;

    /**
     * The cell's building.
     */
    @Expose
    private Building building;

    /**
     * Constructor: initializes a cell containing the land at the position.
     *
     * @param pos  the positon of the cell
     * @param land the land of the cell
     */
    public Cell(Position pos, Land land) {
        this.pos = pos;
        this.land = land;
    }

    /**
     * Returns the cell's position.
     *
     * @return the position of the cell
     */
    public Position getPos() {
        return pos;
    }

    /**
     * Returns the cell's land.
     *
     * @return the land of the cell
     */
    public Land getLand() {
        return land;
    }

    /**
     * Set the cell's land.
     *
     * @param land the new land
     */
    public void setLand(Land land) {
        this.land = land;
    }

    /**
     * Returns the cell's building.
     *
     * @return the building
     */
    public Building getBuilding() {
        return building;
    }

    /**
     * Set the cell's building.
     *
     * @param building the new building
     */
    public void setBuilding(Building building) {
        this.building = building;
    }

    @Override
    public String toString() {
        return "pos:(" + this.pos + ")," + "land:(" + this.land + ")";
    }
}
