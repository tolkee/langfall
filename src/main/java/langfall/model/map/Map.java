package langfall.model.map;

import com.google.gson.annotations.Expose;
import java.io.InputStream;
import java.io.InputStreamReader;
import langfall.Config;
import langfall.exceptions.InvalidTemplateException;
import langfall.exceptions.UnknownIDException;
import langfall.model.land.Forest;
import langfall.model.land.Grass;
import langfall.model.land.Land;
import langfall.model.land.LandID;
import langfall.model.land.Mountain;
import langfall.model.land.River;
import langfall.model.utils.Position;

/**
 * Class representing the map of a game.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class Map {

    /**
     * The width of a map (equivalent to the number of columns).
     */
    public static int WIDTH = 20;

    /**
     * The height of a map (equivalent to the number of rows).
     */
    public static int HEIGHT = 15;

    /**
     * The map's cells.
     */
    @Expose
    private final Cell[][] cells;

    /**
     * Constructor: initializes a map. (you need to load a template or an existing map (next
     * version) after calling the constructor)
     *
     * @see Map#loadTemplate(String)
     */
    public Map() {
        this.cells = new Cell[HEIGHT][WIDTH];
    }

    /**
     * Returns the cell from the map corresponding to the given pos.
     *
     * @param pos the position of the cell
     * @return The corresponding cell or null if the coordinates are not valid.
     */
    public Cell getCell(Position pos) {
        return this.getCell(pos.getX(), pos.getY());
    }

    /**
     * Returns the cell from the map that has for coordinates x and y.
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @return The corresponding cell or null if the coordinates are not valid.
     */
    public Cell getCell(int x, int y) {
        if (x < Map.WIDTH && x >= 0
            && y < Map.HEIGHT && y >= 0) {
            return cells[y][x];
        }
        return null;
    }

    /**
     * Load a template on the map (careful: loadTemplate overwrites existing cell data).
     *
     * @param name The name of the template file.
     * @throws InvalidTemplateException if the given template is invalid
     */
    public void loadTemplate(String name) throws InvalidTemplateException {

        // load and parse template's json file
        InputStream stream = getClass().getResourceAsStream("/maps/" + name);
        InputStreamReader reader = new InputStreamReader(stream);
        int[][] rows = Config.GSON.fromJson(reader, int[][].class);

        // check number of rows
        if (rows.length != HEIGHT) {
            throw new InvalidTemplateException("Template should have " + HEIGHT + " rows");
        }

        // create all cells according to the template
        for (int y = 0; y < HEIGHT; y++) {
            int[] row = rows[y];

            // check length of row (number of columns)
            if (row.length != WIDTH) {
                throw new InvalidTemplateException(
                    "Template rows should have a length equal to " + WIDTH);
            }

            for (int x = 0; x < WIDTH; x++) {
                try {
                    Land land = landFromID(row[x]);
                    cells[y][x] = new Cell(new Position(x, y), land);
                } catch (UnknownIDException err) {
                    throw new InvalidTemplateException(err.getMessage(), err);
                }
            }
        }
    }

    /**
     * Returns a land instance according to the given id.
     *
     * @param id land's id.
     * @return an instance of the land.
     * @throws UnknownIDException If given id doesn't exist on LandID
     * @see LandID
     */
    private Land landFromID(int id) throws UnknownIDException {
        switch (id) {
            case LandID.GRASS:
                return new Grass();
            case LandID.FOREST:
                return new Forest();
            case LandID.MOUTAIN:
                return new Mountain();
            case LandID.RIVER:
                return new River();
            default:
                throw new UnknownIDException("No land corresponding to the ID : " + id);
        }
    }

    @Override
    public String toString() {
        String st = "";
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                st += "[" + getCell(x, y) + "]";
            }
            st += "\n";
        }
        return st;
    }
}
