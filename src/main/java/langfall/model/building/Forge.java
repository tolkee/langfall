package langfall.model.building;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import langfall.model.elements.CraftableElements;
import langfall.model.elements.Element;
import langfall.model.elements.Resources;

/**
 * Class representing a forge.
 *
 * @author Blasius Marine
 * @version 1.0
 */

public class Forge extends Building {

    /**
     * Constructor: initializes a forge building.
     */
    public Forge() {
        super("Forge",
            new ArrayList<>(
                Arrays.asList(CraftableElements.GOLDBAR, CraftableElements.GOLDENBOARD)),
            "A building that can forge rectangular shaped golden things.");
    }

    @Override
    public Map<Element, Integer> getRecipe() {
        Map<Element, Integer> recipe = new HashMap<>();
        recipe.put(Resources.WOOD, 2);
        recipe.put(Resources.ORE, 5);
        return recipe;
    }
}
