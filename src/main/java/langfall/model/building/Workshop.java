package langfall.model.building;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import langfall.model.elements.CraftableElements;
import langfall.model.elements.Element;
import langfall.model.elements.Resources;

/**
 * Class representing a workshop.
 *
 * @author Blasius Marine
 * @version 1.0
 */
public class Workshop extends Building {

    /**
     * Constructor: initializes a workshop building.
     */
    public Workshop() {
        super("Workshop", new ArrayList<>(Arrays.asList(CraftableElements.GOLDENBOARD)),
            "You can't do much in here, but you can still craft beautiful things.");
    }

    @Override
    public Map<Element, Integer> getRecipe() {
        Map<Element, Integer> recipe = new HashMap<>();
        recipe.put(Resources.WOOD, 3);
        recipe.put(Resources.ORE, 1);
        return recipe;
    }
}
