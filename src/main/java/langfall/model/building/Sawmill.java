package langfall.model.building;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import langfall.model.elements.CraftableElements;
import langfall.model.elements.Element;
import langfall.model.elements.Resources;

/**
 * Class representing a sawmill.
 *
 * @author Blasius Marine
 * @version 1.0
 */

public class Sawmill extends Building {

    /**
     * Constructor: initializes a sawmill building.
     */
    public Sawmill() {
        super("Sawmill",
            new ArrayList<>(Arrays.asList(CraftableElements.BOARD, CraftableElements.BRIDGE)),
            "Here to help you craft wooden objects.");
    }

    @Override
    public Map<Element, Integer> getRecipe() {
        Map<Element, Integer> recipe = new HashMap<>();
        recipe.put(Resources.WOOD, 1);
        recipe.put(Resources.ORE, 3);
        return recipe;
    }
}
