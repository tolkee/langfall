package langfall.model.building;

import java.util.HashMap;
import java.util.Map;
import langfall.model.elements.CraftableElements;
import langfall.model.elements.Element;

/**
 * Class representing a bridge.
 *
 * @author Blasius Marine
 * @version 1.0
 */
public class Bridge extends Building {

    /**
     * Constructor: Initializes a bridge.
     */
    public Bridge() {
        super("Bridge", null,
            "A bridge that allows you to cross a river.");
    }

    @Override
    public Map<Element, Integer> getRecipe() {
        Map<Element, Integer> recipe = new HashMap<>();
        recipe.put(CraftableElements.BRIDGE, 1);
        return recipe;
    }
}
