package langfall.model.building;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import langfall.events.InventoryEvent;
import langfall.exceptions.InvalidConceptionException;
import langfall.model.Craftable;
import langfall.model.Player;
import langfall.model.elements.CraftableElements;

/**
 * Class representing a building.
 *
 * @author Blasius Marine
 * @version 3.0
 */
public abstract class Building implements Craftable {

    /**
     * The building's name.
     */
    private final String name;

    /**
     * The list of the craftable elements that this building can produce.
     */
    private final List<CraftableElements> craftableElements;

    /**
     * The building's description.
     */
    private final String description;

    /**
     * Constructor: initializes a building with the given name.
     *
     * @param name              the name of the building
     * @param craftableElements the list of craftable elements for this building
     * @param description       the description of the building
     */
    public Building(String name, List<CraftableElements> craftableElements, String description) {
        this.name = name;
        this.craftableElements = craftableElements;
        this.description = description;
    }

    /**
     * Returns the building's name.
     *
     * @return the building's name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Returns the building's description.
     *
     * @return the building's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the list of craftable elements for this building.
     *
     * @return the list of craftable elements
     */
    public List<CraftableElements> getCraftableElements() {
        return craftableElements != null ? Collections.unmodifiableList(craftableElements) : null;
    }

    /**
     * Crafts the element and adds it to the player's inventory if he has enough elements to
     * complete the recipe.
     *
     * @param craftableElement the element to craft
     * @param player           the player which obtains the element
     */
    public void craft(CraftableElements craftableElement, Player player) {
        if (this.craftableElements.contains(craftableElement) && player
            .removeElement(craftableElement.getRecipe())) {
            player.addElement(craftableElement, 1);
            player.getGame().dispatchEvent(new InventoryEvent());
        }
    }

    /**
     * Returns a new instance of the building class.
     *
     * @param buildingClass the class of the new instance
     * @return an new instance of the class
     * @throws InvalidConceptionException if a problem occurs during the invocation, meaning that a
     *                                    building class does not contain an empty constructor.
     */
    public final static Building getBuildingFromCLass(Class<? extends Building> buildingClass)
        throws InvalidConceptionException {
        try {
            return buildingClass.getConstructor().newInstance();
        } catch (NoSuchMethodException | InstantiationException
            | IllegalAccessException | InvocationTargetException e) {
            // None of those are supposed to happen if the classes are correctly implemented
            throw new InvalidConceptionException(
                "Building class does not possess a valid constructor", e);
        }
    }
}
