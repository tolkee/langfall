package langfall.model;

import langfall.model.elements.Element;

/**
 * Interface for elements that can be crafted.
 *
 * @author Marine Blasius
 * @version 1.0
 */

public interface Craftable {

    /**
     * Gets the recipe of the element.
     *
     * @return a map associating a resource class to a quantity
     */
    java.util.Map<Element, Integer> getRecipe();

    /**
     * Returns the craftable element's name.
     *
     * @return the name
     */
    String getName();

}
