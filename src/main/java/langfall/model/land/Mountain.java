package langfall.model.land;

import langfall.model.elements.Resources;

/**
 * Class representing a mountain land.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 * @see LandID#MOUTAIN
 */

public class Mountain extends FarmableLand {

    /**
     * Constructor: initializes a mountain land.
     */
    public Mountain() {
        super("Mountain", Resources.ORE);
    }

    @Override
    public int getMaxDurability() {
        return 40;
    }

    @Override
    public boolean isPassable() {
        return false;
    }

    @Override
    public boolean isBuildable() {
        return false;
    }
}
