package langfall.model.land;

import com.google.gson.annotations.Expose;
import java.util.List;
import langfall.exceptions.InsufficientResourcesException;
import langfall.model.building.Building;
import langfall.model.elements.Resources;

/**
 * Class representing a farmable land, which can be farmed to obtain resources.
 *
 * @author Blasius Marine
 * @version 4.0
 */

public abstract class FarmableLand extends Land {

    /**
     * The current quantity of the land's resources.
     */
    @Expose
    protected int durability;

    /**
     * The resource that this land produces.
     */
    protected Resources resource;

    /**
     * Constructor: initializes a land with it's name and the name of the farmable resource, and
     * with no buildable building.
     *
     * @param name the name of the land
     */
    public FarmableLand(String name, Resources resource) {
        super(name, null);
        this.resource = resource;
        this.durability = this.getMaxDurability();
    }

    /**
     * Constructor: Initializes a land with it's name and the name of the farmable resource, and the
     * list of the buildables buildings' classes.
     *
     * @param name       the name of the land
     * @param resource   the resource that can be farmed
     * @param buildables the buildables buildings' classes
     */
    public FarmableLand(String name, Resources resource,
        List<Class<? extends Building>> buildables) {
        super(name, buildables);
        this.resource = resource;
        this.durability = this.getMaxDurability();
    }

    /**
     * Returns the maximum quantity of resources for the land.
     *
     * @return the current durability.
     */
    public int getDurability() {
        return durability;
    }

    /**
     * Returns the maximum durability of this land.
     *
     * @return the maximum quantity of resources for the land.
     */
    public abstract int getMaxDurability();

    /**
     * Returns the farmable resource's name.
     *
     * @return the farmable resource's name
     */
    public String getResourceName() {
        return this.resource.getNameElement();
    }

    /**
     * Sets the durability of the land to the maximum durability.
     */
    public void regenerate() {
        this.durability = getMaxDurability();
    }

    /**
     * Farms this land, returning it's associated resource.
     *
     * @return a resource
     * @throws InsufficientResourcesException if the land does not have any more resources.
     */
    public Resources farm() throws InsufficientResourcesException {
        if (durability == 0) {
            throw new InsufficientResourcesException(
                "This land does not have any more resources.");
        }
        durability--;
        return resource;
    }
}
