package langfall.model.land;

import java.util.Arrays;
import langfall.model.building.Forge;
import langfall.model.building.Sawmill;
import langfall.model.building.Workshop;

/**
 * Class representing a grass land.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 * @see LandID#GRASS
 */

public class Grass extends Land {

    /**
     * Constructor: initializes a grass land.
     */
    public Grass() {
        super("Grass", Arrays.asList(Forge.class, Sawmill.class, Workshop.class));
    }

    @Override
    public boolean isPassable() {
        return true;
    }

    @Override
    public boolean isBuildable() {
        return true;
    }
}
