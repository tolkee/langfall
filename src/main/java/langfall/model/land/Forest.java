package langfall.model.land;

import java.util.Arrays;
import langfall.model.building.Sawmill;
import langfall.model.elements.Resources;

/**
 * Class representing a forest land.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 * @see LandID#FOREST
 */

public class Forest extends FarmableLand {

    /**
     * Constructor: initializes a forest land.
     */
    public Forest() {
        super("Forest", Resources.WOOD, Arrays.asList(Sawmill.class));
    }

    @Override
    public boolean isPassable() {
        return durability == 0;
    }

    @Override
    public boolean isBuildable() {
        return durability == 0;
    }

    @Override
    public int getMaxDurability() {
        return 20;
    }
}
