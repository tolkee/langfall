package langfall.model.land;

import java.util.Collections;
import java.util.List;
import langfall.model.building.Building;

/**
 * Abstract class representing a land.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public abstract class Land {

    /**
     * The land's name.
     */
    private final String name;

    /**
     * The set of buildings that are buildable on this land.
     */
    protected final List<Class<? extends Building>> buildables;

    /**
     * Constructor: initializes a land with the given name and indicates which buildings are
     * buildable on this land.
     *
     * @param name       The name of the land
     * @param buildables set of buildable buildings classes
     */
    public Land(String name, List<Class<? extends Building>> buildables) {
        this.name = name;
        this.buildables = buildables;
    }

    /**
     * Returns the land's name.
     *
     * @return the land's name
     */
    public String getName() {
        return name;
    }

    /**
     * Determines whether the land is passable or not.
     *
     * @return true if the land is passable, false otherwise
     */
    public abstract boolean isPassable();

    /**
     * Determines if a building can be build on that land.
     *
     * @return true if a building can be built, false otherwise
     */
    public abstract boolean isBuildable();

    /**
     * Determines if the building is buildable on this land.
     *
     * @param building the building class to check
     * @return true if it is buildable, false otherwise
     */
    public boolean isBuildable(Class<? extends Building> building) {
        return isBuildable() && buildables.contains(building);
    }

    /**
     * Returns the buildings' classes that are buildable on this land.
     *
     * @return the buildings' classes
     */
    public List<Class<? extends Building>> getBuildables() {
        return this.buildables != null ?
            Collections.unmodifiableList(this.buildables) : null;
    }

    @Override
    public String toString() {
        return this.name + "," + (isPassable() ? "Passable" : "not Passable");
    }
}
