package langfall.model.land;

/**
 * Class containing the id of all existing lands (use especially for parsing json of Langfall map
 * ).
 *
 * @author Guillaume Lacoste
 * @version 1.0
 * @see langfall.model.map.Map#loadTemplate(String)
 */

public class LandID {

    /**
     * Grass land ID
     *
     * @see Grass
     */
    public static final int GRASS = 1;

    /**
     * Forest land ID
     *
     * @see Forest
     */
    public static final int FOREST = 2;

    /**
     * Mountain land ID
     *
     * @see Mountain
     */
    public static final int MOUTAIN = 3;

    /**
     * River land ID
     *
     * @see River
     */
    public static final int RIVER = 4;
}
