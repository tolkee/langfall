package langfall.model.land;

import java.util.Arrays;
import langfall.model.building.Bridge;
import langfall.model.elements.Resources;

/**
 * Class representing a water land.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 * @see LandID#RIVER
 */

public class River extends FarmableLand {

    /**
     * Constructor: initializes a river land.
     */
    public River() {
        super("River", Resources.WATER, Arrays.asList(Bridge.class));
    }

    @Override
    public boolean isPassable() {
        return false;
    }

    @Override
    public boolean isBuildable() {
        return true;
    }

    @Override
    public int getMaxDurability() {
        return -1;
    }

    @Override
    public Resources farm() {
        return this.resource;
    }
}
