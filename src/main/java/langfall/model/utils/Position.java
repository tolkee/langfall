package langfall.model.utils;

import com.google.gson.annotations.Expose;
import java.util.Objects;

/**
 * Class representing a position in Langfall.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class Position implements Cloneable {

    /**
     * Coordinates x and y.
     */
    @Expose
    private int x, y;

    /**
     * Constructor: initialize a position with the given coordinates x and y.
     *
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Adds given coordinates x and y to the position.
     *
     * @param x the x coordinate to add
     * @param y the y coordinate to add
     */
    public void add(int x, int y) {
        this.x += x;
        this.y += y;
    }

    /**
     * Adds a given position to the position.
     *
     * @param pos the position to add
     */
    public void add(Position pos) {
        this.add(pos.x, pos.y);
    }

    /**
     * Subtracts the given coordinates x and y to the position.
     *
     * @param x the x coordinate to subtract
     * @param y the y coordinate to subtract
     */
    public void subtract(int x, int y) {
        this.x -= x;
        this.y -= y;
    }

    /**
     * Return the coordinate x of the pos.
     *
     * @return The coordinate x of the pos.
     */
    public int getX() {
        return x;
    }

    /**
     * Return the coordinate y of the pos.
     *
     * @return The coordinate y of the pos.
     */
    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Position position = (Position) o;
        return x == position.x &&
            y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "x=" + this.x + ",y=" + this.y;
    }

    @Override
    public Position clone() {
        return new Position(x, y);
    }
}
