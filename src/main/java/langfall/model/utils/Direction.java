package langfall.model.utils;

/**
 * Enum containing all directions possibles in Langfall.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public enum Direction {
    SOUTH, NORTH, EAST, WEST
}
