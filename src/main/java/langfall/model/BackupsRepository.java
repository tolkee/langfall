package langfall.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import langfall.Config;

/**
 * Class containing methods to interact with the backups repository.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class BackupsRepository {

    /**
     * The name of the backups repository.
     */
    public static final String repositoryName = "backups";

    /**
     * Return all the name of the backups contained in the backups repository. Create the backups
     * repository if it doesn't exist.
     *
     * @return Return a list of the backup's name
     */
    public static List<String> getAllBackupsName() {
        File repository = checkBackupsRepository();
        List<File> backups = Arrays.asList(
            Objects.requireNonNull(repository.listFiles(((dir, name) -> name.endsWith(".json")))));

        List<String> backupsName = new ArrayList<>();
        backups.forEach((file -> backupsName.add(toGameTitle(file.getName()))));

        return backupsName;
    }

    /**
     * Load and return from the backups repository a given backup. Create the backups repository if
     * it doesn't exist.
     *
     * @param backupName The name of the backup
     * @return Return the loaded game from the backup
     * @throws IOException If the backup doesn't exist or if no write access rights on the backup
     *                     repository or the file
     */
    public static Game loadBackup(String backupName) throws IOException {
        checkBackupsRepository();

        File backupFile = new File(toFileName(backupName));

        if (!(backupFile.exists() && backupFile.isFile())) {
            throw new FileNotFoundException(backupName + " not founds in backups directory");
        }

        Reader reader = new FileReader(backupFile);

        Game backup = Config.GSON.fromJson(reader, Game.class);
        backup.init();

        reader.close();

        return backup;
    }


    /**
     * Save the given game in its backup (it creates it if there is none). Create the backups
     * repository if it doesn't exist.
     *
     * @param game The game to save on the backup
     * @throws IOException If no write access rights on the backup repository or the file
     */
    public static void saveBackup(Game game) throws IOException {
        game.setSavedAt(new Date());
        checkBackupsRepository();

        File backupFile = new File(
            toFileName(game.getTitle()));

        if (!(backupFile.exists() && backupFile.isFile())) {
            backupFile.createNewFile();
        }

        Writer writer = new FileWriter(backupFile);
        Config.GSON.toJson(game, writer);

        writer.close();
    }

    /**
     * Delete a given backup. Create the backups repository if it doesn't exist.
     *
     * @param backupName The name of the backup to delete
     */
    public static void deleteBackup(String backupName) {
        checkBackupsRepository();

        File backupFile = new File(toFileName(backupName));

        if (backupFile.exists() && backupFile.isFile()) {
            backupFile.delete();
        }
    }

    /**
     * Check if the backups repository exist. If not, it create it.
     * <p>
     * Then it return the repository.
     *
     * @return return the backup repository
     */
    private static File checkBackupsRepository() {
        File repository = new File(repositoryName);

        if (!(repository.exists() && repository.isDirectory())) {
            repository.mkdir();
        }

        return repository;
    }

    /**
     * Converts the game title to a valid file name.
     *
     * @param gameTitle the game title
     * @return a valid file name
     */
    private static String toFileName(String gameTitle) {
        return BackupsRepository.repositoryName + "/" + gameTitle.replace(" ", "_") + ".json";
    }

    /**
     * Converts a file name to a game title.
     *
     * @param fileName the file name
     * @return the game title
     */
    private static String toGameTitle(String fileName) {
        return fileName.replace(".json", "").replace("_", " ");
    }

}
