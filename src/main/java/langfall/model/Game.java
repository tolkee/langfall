package langfall.model;

import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import langfall.events.Dispatcher;
import langfall.exceptions.InvalidTemplateException;
import langfall.model.map.Map;
import langfall.model.utils.Position;
import langfall.tasks.BackgroundTask;
import langfall.tasks.RegenerateResourcesTask;

/**
 * Class representing a game of Langfall.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class Game extends Dispatcher {

    /**
     * The player of the game
     */
    @Expose
    private Player player;

    /**
     * The map of the game
     */
    @Expose
    private Map map;

    /**
     * The title of the game
     */
    @Expose
    private String title;

    /**
     * The date of the game's creation
     */
    @Expose
    private Date createdAt;

    /**
     * The date of the last save
     */
    @Expose
    private Date savedAt;

    /**
     * Background tasks to be performed by the game.
     */
    private List<BackgroundTask> gameTasks;

    /**
     * Constructor: Initialize a game with a template of the map.
     *
     * @param title    The name of the new game
     * @param template The template of the map
     * @throws InvalidTemplateException if the given template is invalid
     */
    public Game(String title, String template)
        throws InvalidTemplateException {
        super();
        this.title = title;

        Position initialPositionPlayer =
            new Position((Map.WIDTH) / 2, (Map.HEIGHT) / 2);
        this.player = new Player("Cram Letnap", initialPositionPlayer, this);
        initialPositionPlayer.add(1, 0);
        this.map = new Map();
        this.createdAt = new Date();
        map.loadTemplate(template);
    }

    /**
     * Method called even if the instance is created by GSON. Useful because during the
     * deserialization, GSON doesn't call the class's constructor to create an instance.
     */
    @Override
    public void init() {
        super.init();
        if (this.player != null) {
            this.player.setGame(this);
        }
        this.gameTasks = new ArrayList<>();
        this.gameTasks.add(new RegenerateResourcesTask(this));
    }

    public void start() {
        this.gameTasks.forEach(BackgroundTask::start);
    }


    public void stop() {
        this.gameTasks.forEach(BackgroundTask::stop);
    }

    /**
     * Return the player of the game.
     *
     * @return The player of the game.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Set the player of the game.
     *
     * @param player The new player.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Return the map of the game.
     *
     * @return The map of the game.
     */
    public Map getMap() {
        return map;
    }

    /**
     * Set the map of the game.
     *
     * @param map The new map.
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * Return the title of the game.
     *
     * @return The title of the game.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Set the date of the last save.
     *
     * @param savedAt The new date.
     */
    public void setSavedAt(Date savedAt) {
        this.savedAt = savedAt;
    }
}
