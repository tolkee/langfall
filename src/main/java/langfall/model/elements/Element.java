package langfall.model.elements;

/**
 * Interface of an element which can be used in recipes.
 *
 * @author Blasius Marine
 * @version 1.0
 */
public interface Element {

    /**
     * Returns the element's name.
     *
     * @return the element's name
     */
    default String getNameElement() {
        return this.toString().substring(0, 1).toUpperCase() + this.toString().substring(1)
            .toLowerCase();
    }
}
