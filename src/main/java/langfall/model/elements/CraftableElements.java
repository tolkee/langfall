package langfall.model.elements;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import langfall.model.Craftable;

/**
 * The enum containing the craftable elements.
 *
 * @author Blasius Marine
 * @version 2.0
 */
public enum CraftableElements implements Element, Craftable {
    GOLDBAR {
        @Override
        public Map<Element, Integer> getRecipe() {
            Map<Element, Integer> recipe = new TreeMap<>();
            recipe.put(Resources.ORE, 10);
            recipe.put(Resources.WATER, 5);
            return recipe;
        }
    },
    BOARD {
        @Override
        public Map<Element, Integer> getRecipe() {
            Map<Element, Integer> recipe = new HashMap<>();
            recipe.put(Resources.WOOD, 5);
            return recipe;
        }
    },
    GOLDENBOARD {
        @Override
        public Map<Element, Integer> getRecipe() {
            Map<Element, Integer> recipe = new HashMap<>();
            recipe.put(CraftableElements.GOLDBAR, 1);
            recipe.put(CraftableElements.BOARD, 1);
            recipe.put(Resources.WATER, 3);
            return recipe;
        }
    },
    BRIDGE {
        @Override
        public Map<Element, Integer> getRecipe() {
            Map<Element, Integer> recipe = new HashMap<>();
            recipe.put(Resources.WOOD, 5);
            recipe.put(CraftableElements.BOARD, 3);
            return recipe;
        }
    };

    @Override
    public String getName() {
        return getNameElement();
    }
}
