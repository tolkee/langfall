package langfall.model.elements;

/**
 * Enum containing basic resources (not craftable).
 *
 * @author Blasius Marine
 * @version 1.0
 */

public enum Resources implements Element {
    ORE,
    WATER,
    WOOD
}
