package langfall.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import langfall.view.stages.ControlsStage;

/**
 * Class containing the config of the UI components
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class UIConfig {

    /**
     * The boolean property representing if the grid of the map is visible or not.
     */
    public static final BooleanProperty mapGridVisible = new SimpleBooleanProperty(false);

    /**
     * The interface for binding keys to controls.
     */
    public static final ControlsStage controlsUI = new ControlsStage();
}
