package langfall.view.stages;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import langfall.view.ImageLoader;

/**
 * The UI for choosing the key binding.
 */
public class ControlsStage extends Stage {

    /**
     * The controls associated with their button.
     */
    private final Map<Controls, Button> controls = new TreeMap<>();

    /**
     * The pane containing the controls.
     */
    private final GridPane controlsContainer = new GridPane();

    /**
     * The selected control.
     */
    private Controls currentControl;

    /**
     * The selected button.
     */
    private Button currentControlButton;

    /**
     * Initializes the controls interface.
     */
    public ControlsStage() {
        super();
        setDefaultControls();
        this.setTitle("Controls");
        this.getIcons().add(ImageLoader.getImage("Settings"));
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);

        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);

        root.setPadding(new Insets(10));

        controlsContainer.setHgap(15);
        controlsContainer.setVgap(15);

        AtomicInteger rowNumber = new AtomicInteger(0);

        controls.forEach((control, buttonKey) -> {
            Label actionLabel = new Label(control.getAction());
            controlsContainer.add(actionLabel, 0, rowNumber.get());

            buttonKey.setOnAction(event -> {
                currentControl = control;
                if (currentControlButton != null) {
                    currentControlButton.setStyle("-fx-background-color: #202426");
                }
                currentControlButton = buttonKey;
                currentControlButton.setStyle("-fx-background-color: #E79B74");

            });

            controlsContainer.add(buttonKey, 1, rowNumber.get());
            rowNumber.getAndIncrement();
        });

        root.getChildren().addAll(controlsContainer);

        Scene scene = new Scene(root);

        scene.setOnKeyPressed(event -> updateControl(event.getCode()));

        this.setScene(scene);
    }

    /**
     * Gets the key code associated with the control.
     *
     * @param control the control
     * @return the key code
     */
    public KeyCode getControl(Controls control) {
        try {
            return KeyCode.valueOf(this.controls.get(control).getText());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    /**
     * Sets the default configuration for the controls.
     */
    public void setDefaultControls() {
        controls.put(Controls.BUILD, getButtonFromKey(KeyCode.C));
        controls.put(Controls.FARM, getButtonFromKey(KeyCode.F));
        controls.put(Controls.LEFT, getButtonFromKey(KeyCode.Q));
        controls.put(Controls.RIGHT, getButtonFromKey(KeyCode.D));
        controls.put(Controls.UP, getButtonFromKey(KeyCode.Z));
        controls.put(Controls.DOWN, getButtonFromKey(KeyCode.S));
    }

    /**
     * Binds the key to the currently selected control.
     *
     * @param newKey the key to bind
     */
    private void updateControl(KeyCode newKey) {
        if (currentControl != null) {
            controls.forEach((control, buttonKey) -> {
                if (getControl(control) == newKey) {
                    controls.get(control).setText("");
                    controls.get(control).setStyle("-fx-background-color: red");
                }
            });
            currentControlButton.setText(newKey.toString());
            currentControlButton.setStyle("-fx-background-color: #202426");
        }
    }

    /**
     * Returns the button corresponding to the key.
     *
     * @param key the key code
     * @return the button associated
     */
    private Button getButtonFromKey(KeyCode key) {
        Button button = new Button(key.toString());
        button.setPrefSize(80, 30);
        button.setTextFill(Color.WHITE);
        button.setStyle("-fx-background-color: #202426");
        button.setCursor(Cursor.OPEN_HAND);
        return button;
    }

    /**
     * The controls available in the game.
     */
    public enum Controls {
        BUILD, FARM, LEFT, RIGHT, UP, DOWN;

        public String getAction() {
            return this.toString().substring(0, 1).toUpperCase() + this.toString().substring(1)
                .toLowerCase();
        }
    }
}
