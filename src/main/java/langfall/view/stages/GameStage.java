package langfall.view.stages;

import java.io.IOException;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import langfall.model.BackupsRepository;
import langfall.model.Game;
import langfall.view.ImageLoader;
import langfall.view.scenes.GameScene;

/**
 * The main game stage.
 */
public class GameStage extends Stage {

    /**
     * Initializes the game stage.
     *
     * @param game the game for this stage
     */
    public GameStage(Game game) {
        super();

        // start the game
        game.start();

        this.setTitle("Langfall - " + game.getTitle());
        this.getIcons().add(ImageLoader.getImage("Logo"));
        this.setScene(new GameScene(game));
        this.setResizable(false);

        // Quit dialog
        Alert quitDialog = new Alert(AlertType.CONFIRMATION);
        quitDialog.setTitle("Exit Langfall");
        quitDialog.setHeaderText("You are going to quit the game.");
        ButtonType saveExit = new ButtonType("Save and exit");
        ButtonType exit = new ButtonType("Exit");
        ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        quitDialog.getButtonTypes().setAll(saveExit, exit, cancel);

        this.setOnCloseRequest(event -> {
            Optional<ButtonType> dialogResult = quitDialog.showAndWait();
            if (dialogResult.get() == saveExit) {
                try {
                    BackupsRepository.saveBackup(game);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.exit(0);
            } else if (dialogResult.get() == exit) {
                System.exit(0);
            } else {
                event.consume();
            }
        });

        // when java shutdown, stop the current game
        Runtime.getRuntime().addShutdownHook(new Thread(game::stop));
    }
}
