package langfall.view.components.info;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import langfall.model.Player;
import langfall.model.building.Building;
import langfall.model.elements.CraftableElements;
import langfall.view.ImageLoader;
import langfall.view.Langfall;

/**
 * The craft user interface.
 */
public class CraftUI extends Popup {

    /**
     * Width of the popup.
     */
    private final double WIDTH = 400;

    /**
     * Height of the popup.
     */
    private final double HEIGHT = 300;

    /**
     * Constructor: initializes a craft UI for the player, with the craftable elements from the
     * building.
     *
     * @param building the building
     * @param player   the player
     */
    public CraftUI(Building building, Player player) {
        super();
        this.setAnchorX(
            Langfall.primaryStage.getX() + (Langfall.primaryStage.getWidth() / 2 - WIDTH / 2));
        this.setAnchorY(Langfall.primaryStage.getY() + (Langfall.primaryStage.getHeight() / 2
            - HEIGHT / 2));

        VBox root = new VBox();
        root.setStyle("-fx-background-color: #272726;");
        root.setPrefSize(WIDTH, HEIGHT);

        Label labelTitle = InfoUI.labelWithLogo("CraftTool", "Craft", 25);
        labelTitle.setPadding(new Insets(7, 0, 7, 10));

        ListView<RecipeUI> listCrafts = new ListView<>();

        building.getCraftableElements()
            .forEach(element -> listCrafts.getItems().add(new RecipeUI(element, player)));

        HBox buttonContainer = new HBox();
        buttonContainer.setAlignment(Pos.CENTER);
        buttonContainer.setPadding(new Insets(10, 0, 10, 0));

        Button craftButton = CraftUI.craftButton("Craft");
        // enable button only if an item is selected
        craftButton.disableProperty()
            .bind(listCrafts.getSelectionModel().selectedItemProperty().isNull());
        craftButton.setOnMouseClicked(event -> {
            RecipeUI el = listCrafts.getSelectionModel().getSelectedItem();
            if (el != null && !el.isDisable()) {
                building.craft((CraftableElements) el.getCraftable(), player);
                listCrafts.getItems().forEach(RecipeUI::updateStatus);
            }
        });

        buttonContainer.getChildren().add(craftButton);
        root.getChildren().addAll(labelTitle, listCrafts, buttonContainer);

        this.getContent().add(root);
        this.setAutoHide(true);
    }

    /**
     * Returns a button with a craft icon and the given text.
     *
     * @param text the text on the button
     * @return a button
     */
    public static Button craftButton(String text) {
        Button craftButton = new Button(text);
        ImageView craftIcon = new ImageView(ImageLoader.getImage("Craft"));
        craftIcon.setFitWidth(20);
        craftIcon.setFitHeight(20);
        craftButton.setGraphic(craftIcon);
        craftButton.setCursor(Cursor.OPEN_HAND);
        craftButton.setStyle(
            "-fx-background-color: #ebae34; -fx-font-size: 12; -fx-font-weight: bold");
        craftButton.setAlignment(Pos.CENTER);
        return craftButton;
    }
}
