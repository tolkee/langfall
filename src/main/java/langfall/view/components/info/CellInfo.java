package langfall.view.components.info;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import langfall.model.Player;
import langfall.model.building.Building;
import langfall.model.land.FarmableLand;
import langfall.model.land.Land;
import langfall.model.map.Cell;

/**
 * The information UI for a cell.
 */
public class CellInfo extends VBox {

    /**
     * The cell for whom we wish to display information.
     */
    private final Cell cell;

    /**
     * The player of the game.
     */
    private final Player player;

    /**
     * Constructor: initializes a cell information.
     *
     * @param cell   the cell for whom we wish to display information.
     * @param player the player
     */
    public CellInfo(Cell cell, Player player) {
        this.cell = cell;
        this.player = player;
        this.setPrefWidth(50);
        this.setPadding(new Insets(50, 0, 0, 0));

        BuildingInfo buildingInfo = new BuildingInfo(cell.getBuilding());
        LandInfo landInfo = new LandInfo(cell.getLand());
        this.getChildren().addAll(landInfo, buildingInfo);
    }

    /**
     * Returns the cell corresponding to this cell info.
     *
     * @return the cell
     */
    public Cell getCell() {
        return this.cell;
    }

    /**
     * Information about the land of the cell
     */
    private static class LandInfo extends VBox {

        public LandInfo(Land land) {
            super();
            this.setPadding(new Insets(5));
            this.setStyle("-fx-background-color: #333333;");

            Label landName = new Label(land.getName());
            landName.setTextFill(Color.web("#F2F2ED"));
            landName.setStyle("-fx-font-weight: bold; -fx-padding: 0 0 15 0;");
            Label landPassable = new Label(
                land.isPassable() ? "Passable" : "Impassable");
            landPassable.setTextFill(Color.web("#F2F2ED"));
            landPassable.setStyle("-fx-padding: 0 0 10 0;");
            this.getChildren()
                .addAll(landName, landPassable);

            if (land instanceof FarmableLand) {
                FarmableLand fLand = (FarmableLand) land;
                Label landFarmable = InfoUI.labelWithLogo("", "", 20, ContentDisplay.RIGHT);
                landFarmable.setWrapText(true);
                landFarmable.setStyle("-fx-padding: 0 0 10 0;");
                InfoUI.setLabelWithLogo(landFarmable, "Resource: ",
                    fLand.getResourceName(), 20, ContentDisplay.RIGHT);
                Label landDurability = new Label();
                landDurability.setTextFill(Color.web("#F2F2ED"));
                landDurability.setText(
                    fLand.getDurability() >= 0 ? fLand.getDurability() + "/" + fLand
                        .getMaxDurability()
                        : "\u221E");

                this.getChildren()
                    .addAll(landFarmable, landDurability);
            }
        }
    }

    /**
     * Information about the building on the cell.
     */
    private class BuildingInfo extends VBox {

        public BuildingInfo(Building building) {
            super();
            this.setStyle("-fx-background-color: #222222;");
            this.setPadding(new Insets(5));

            if (building == null) {
                this.getChildren().add(new Label("No building yet."));
            } else {
                // building name
                Label buildingName = new Label(building.getName());
                buildingName.setTextFill(Color.web("#F2F2ED"));
                buildingName.setStyle("-fx-font-weight: bold;");

                // building description
                Label buildingDescription = new Label(building.getDescription());
                buildingDescription.setWrapText(true);
                buildingDescription.setTextFill(Color.web("#F2F2ED"));
                buildingDescription.setStyle("-fx-padding: 15 0 15 0;");

                this.getChildren()
                    .addAll(buildingName, buildingDescription);

                // craft button
                if (building.getCraftableElements() != null) {
                    Button craftButton = CraftUI.craftButton("CraftTool");
                    if (!player.getPosition().equals(cell.getPos())) {
                        craftButton.setDisable(true);
                    }

                    CraftUI popupCraft = new CraftUI(building, player);

                    craftButton.setOnAction((event -> {
                        if (!popupCraft.isShowing()) {
                            popupCraft.show(this.getScene().getWindow());
                        } else {
                            popupCraft.hide();
                        }
                    }));

                    this.getChildren().add(craftButton);
                }
            }
        }
    }
}
