package langfall.view.components.info;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import langfall.model.Game;
import langfall.model.map.Cell;
import langfall.view.ImageLoader;


/**
 * Class representing the UI component Info
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class InfoUI extends VBox {

    /**
     * The player's information.
     */
    private final PlayerInfo playerInfo;

    /**
     * The cell's information (land and building).
     */
    private CellInfo cellInfo;

    /**
     * The game.
     */
    private final Game game;

    /**
     * Constructor: initializes an infoUI with the game.
     *
     * @param game The game
     */
    public InfoUI(Game game) {
        this.game = game;
        this.setStyle("-fx-padding: 20;");

        playerInfo = new PlayerInfo(game.getPlayer());
        playerInfo.updatePlayerInventory();

        this.getChildren().addAll(playerInfo);
    }


    /**
     * Update the player info
     */
    public void updatePlayerInfo() {
        this.playerInfo.updatePlayerInventory();
    }

    /**
     * Update the cell info
     */
    public void updateCellInfo(Cell newCell) {
        if (this.cellInfo != null) {
            this.getChildren().remove(cellInfo);
        }
        this.cellInfo = new CellInfo(newCell, game.getPlayer());
        this.getChildren().add(this.cellInfo);
    }

    /**
     * Returns a label with the text text and the graphic associated with image name set at size. If
     * the image can't be found, returns the label with the text.
     *
     * @param text      the label's text
     * @param imageName the label's graphic name
     * @param size      the size of the graphic
     * @return a label with a logo
     */
    public static Label labelWithLogo(String text, String imageName, int size) {
        Label name = new Label(text);
        name.setTextFill(Color.web("#F2F2ED"));
        Image image = ImageLoader.getImage(imageName);
        if (image != null) {
            ImageView logo = new ImageView(image);
            logo.setFitHeight(size);
            logo.setFitWidth(size);
            logo.setPreserveRatio(true);
            name.setGraphic(logo);
        }
        return name;
    }

    /**
     * Returns a label with the text text and the graphic associated with image name set at size and
     * disposed following imagePosition. If the image can't be found, returns the label with the
     * text.
     *
     * @param text          the label's text
     * @param imageName     the label's graphic name
     * @param size          the size of the graphic
     * @param imagePosition the image positon
     * @return a label with a logo
     */
    public static Label labelWithLogo(String text, String imageName, int size,
        ContentDisplay imagePosition) {
        Label name = labelWithLogo(text, imageName, size);
        name.setContentDisplay(imagePosition);
        return name;
    }

    /**
     * Sets the label with the text and the graphic associated with image name set at size and
     * disposed following imagePosition.
     *
     * @param label         the label
     * @param text          the text
     * @param imageName     the image name
     * @param size          the size of the image
     * @param imagePosition the image position
     */
    public static void setLabelWithLogo(Label label, String text, String imageName, int size,
        ContentDisplay imagePosition) {
        label.setText(text);
        label.setTextFill(Color.web("#F2F2ED"));
        ImageView playerLogo = new ImageView(ImageLoader.getImage(imageName));
        playerLogo.setFitHeight(size);
        playerLogo.setFitWidth(size);
        playerLogo.setPreserveRatio(true);
        label.setGraphic(playerLogo);
        label.setContentDisplay(imagePosition);
    }


}
