package langfall.view.components.info;

import java.util.HashMap;
import java.util.Map;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.VBox;
import langfall.model.Player;
import langfall.model.elements.Element;

/**
 * UI for information about the player.
 */
public class PlayerInfo extends VBox {

    /**
     * The player.
     */
    private final Player player;

    /**
     * The labels for the inventory.
     */
    private final Map<Element, Label> elementsLabels;

    /**
     * The pan containing the inventory.
     */
    private final VBox inventoryBox;

    /**
     * Constructor: initializes the player information.
     *
     * @param player the player
     */
    public PlayerInfo(Player player) {
        super();
        this.player = player;
        this.setAlignment(Pos.CENTER_LEFT);

        // player name
        Label playerLbl = InfoUI.labelWithLogo(player.getName(), "Player", 30);

        // player inventory
        this.elementsLabels = new HashMap<>();
        this.inventoryBox = new VBox();
        updatePlayerInventory();

        // separator
        Separator separator = new Separator();
        separator.setStyle("-fx-padding: 5 0 10 0;");

        this.getChildren().addAll(playerLbl, separator, inventoryBox);
    }

    /**
     * Update the player's inventory display.
     */
    public void updatePlayerInventory() {
        // Gets all the new resources
        this.player.getInventory().forEach((element, quantity) -> {
            if (!elementsLabels.containsKey(element)) {
                elementsLabels
                    .put(element,
                        InfoUI.labelWithLogo(quantity.toString(), element.toString(), 20));
            }
        });
        this.elementsLabels.forEach((element, label) -> {
            if (this.player.getInventory().containsKey(element)) {
                if (this.inventoryBox.getChildren().contains(label)) {
                    // Update the resources quantity
                    label.setText(this.player.getInventory().get(element).toString());
                } else {
                    // Adds the new label
                    this.inventoryBox.getChildren().add(label);
                }
            } else {
                // Removes label that are no longer used
                this.inventoryBox.getChildren().remove(label);
            }
        });
    }

}
