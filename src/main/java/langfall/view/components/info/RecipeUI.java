package langfall.view.components.info;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import langfall.model.Craftable;
import langfall.model.Player;

/**
 * UI for craftables recipe.
 */
public class RecipeUI extends VBox {

    /**
     * The craftable thing for which the recipe is displayed.
     */
    private final Craftable craftable;

    /**
     * The player (to indicate if he has enough resources compared to the recipe).
     */
    private final Player player;

    /**
     * Constructor: initializes a recipe UI for the craftable and the player.
     *
     * @param craftable the craftable for which the recipe is displayed
     * @param player    the player
     */
    public RecipeUI(Craftable craftable, Player player) {
        super();
        this.craftable = craftable;
        this.player = player;

        final int IMAGE_SIZE = 20;
        Label title = InfoUI
            .labelWithLogo(craftable.getName(), craftable.toString(), IMAGE_SIZE);
        title.setTextFill(Color.BLACK);
        title.setPadding(new Insets(0, 0, 3, 0));

        HBox recipe = new HBox();
        craftable.getRecipe().forEach((element, quantity) ->
            {
                Label recipeItem = InfoUI
                    .labelWithLogo(quantity.toString(), element.toString(),
                        (int) (IMAGE_SIZE / 1.3));
                recipeItem.setTextFill(Color.BLACK);
                recipeItem.setPadding(new Insets(0, 8, 0, 0));
                recipe.getChildren().add(recipeItem);
            }
        );

        this.updateStatus();

        this.getChildren().addAll(title, recipe);
    }

    /**
     * Update the status of the recipe UI: it is disabled if the player doesn't have enough
     * resources in his inventory.
     */
    public void updateStatus() {
        if (!player.hasEnoughElements(craftable.getRecipe())) {
            this.setDisable(true);
        } else {
            this.setDisable(false);
        }
    }

    /**
     * Returns the craftable thing.
     *
     * @return the craftable
     */
    public Craftable getCraftable() {
        return craftable;
    }

}
