package langfall.view.components;

import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import langfall.model.Player;
import langfall.model.building.Building;
import langfall.model.map.Cell;
import langfall.view.ImageLoader;
import langfall.view.components.info.RecipeUI;

/**
 * Class of the building user interface.
 *
 * @author Marine Blasius
 * @version 3.0
 */
public class BuildingUI extends HBox {

    /**
     * The button currently selected.
     */
    private BuildingButton selectedButton;

    /**
     * The group containing all of the possible buildings.
     */
    private ToggleGroup buildingGroup;

    /**
     * Pane containing the building recipe.
     */
    private HBox recipeBox;

    /**
     * The player
     */
    private final Player player;

    /**
     * Constructor: Initializes the building user interface.
     *
     * @param player       the player
     * @param selectedCell the cell currently selected on the map
     */
    public BuildingUI(Player player, Cell selectedCell) {
        super(8);
        this.setPadding(new Insets(8, 8, 8, 8));
        this.setStyle("-fx-background-color: #272726;");
        this.player = player;

        updateBuildings(selectedCell);
    }

    /**
     * Returns the selected building among the building choices.
     *
     * @return the selected building
     */
    public Class<? extends Building> getSelectedBuildingClass() {
        return selectedButton != null ? selectedButton.getBuildingClass() : null;
    }

    /**
     * Updates the recipe status for the selected button.
     */
    public void updateRecipe() {
        this.selectedButton.updateRecipe();
    }

    /**
     * Update the building choice following the buildings that can be build on the cell.
     *
     * @param cell a cell
     */
    public void updateBuildings(Cell cell) {
        this.getChildren().clear();

        List<Class<? extends Building>> buildables = cell.getLand().getBuildables();

        if (buildables != null) {

            List<BuildingButton> buttons = new ArrayList<>();
            for (Class<? extends Building> buildingClass : buildables) {
                buttons.add(new BuildingButton(buildingClass));
            }

            this.buildingGroup = new ToggleGroup();
            for (BuildingButton button : buttons) {
                button.setToggleGroup(this.buildingGroup);
                this.getChildren().add(button);
            }

            this.recipeBox = new HBox();
            this.recipeBox.setStyle("-fx-background-color: #ededed; -fx-border-color: #b0b0b0;");
            this.recipeBox.setPadding(new Insets(8, 8, 8, 8));

            buttons.get(0).clickButton();

            this.getChildren()
                .addAll(new Separator(Orientation.VERTICAL), recipeBox);
        }
    }

    /**
     * Button class corresponding to a building.
     *
     * @author Marine Blasius
     * @version 2.0
     */
    private class BuildingButton extends ToggleButton {

        private final static String STYLE_BUTTON_UNSELECTED =
            "-fx-background-color: #dedede; -fx-border-color: #7a7a7a;";
        private final static String STYLE_BUTTON_SELECTED =
            "-fx-background-color: #ededed; -fx-border-color: #db3327; -fx-border-width: 2px";

        /**
         * The building class embedded in this button.
         */
        private final Class<? extends Building> buildingClass;

        /**
         * The recipe for the building.
         */
        private RecipeUI recipe;

        /**
         * Constructor: Initializes a building button containing the building class.
         *
         * @param buildingClass the building class
         */
        public BuildingButton(Class<? extends Building> buildingClass) {
            super();
            this.buildingClass = buildingClass;
            this.setStyle(STYLE_BUTTON_UNSELECTED);
            int IMAGE_SIZE = 40;
            ImageView image = new ImageView(ImageLoader.getImage(buildingClass.getSimpleName()));
            image.setFitHeight(IMAGE_SIZE);
            image.setFitWidth(IMAGE_SIZE);
            this.setGraphic(image);
            this.setOnAction(event -> clickButton());
        }

        /**
         * Selects this button from his toggle group.
         */
        public void clickButton() {
            this.setStyle(STYLE_BUTTON_SELECTED);
            buildingGroup.getToggles().forEach(toggle -> {
                BuildingButton button = (BuildingButton) toggle;
                if (button != this) {
                    button.setStyle(STYLE_BUTTON_UNSELECTED);
                }
            });

            // Creates craft infos
            recipeBox.getChildren().clear();
            this.recipe = new RecipeUI(Building.getBuildingFromCLass(buildingClass), player);
            recipeBox.getChildren().add(this.recipe);
            selectedButton = this;
        }

        /**
         * Returns the building class associated with this button.
         *
         * @return the corresponding building class
         */
        public Class<? extends Building> getBuildingClass() {
            return buildingClass;
        }

        /**
         * Updates the recipe for the building class in this button.
         */
        public void updateRecipe() {
            this.recipe.updateStatus();
        }
    }
}
