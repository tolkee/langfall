package langfall.view.components;

import javafx.scene.layout.GridPane;
import langfall.model.Game;
import langfall.model.map.Map;
import langfall.model.utils.Position;
import langfall.view.UIConfig;

/**
 * Class representing the UI component Map.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class MapUI extends GridPane {

    /**
     * The cellUIs of the mapUI.
     */
    private final CellUI[][] cells;

    /**
     * The selected cellUI on this map.
     */
    private CellUI selectedCell;

    private Game game;

    /**
     * Constructor: initializes mapUI according to the map of the game.
     *
     * @param game The game
     */
    public MapUI(Game game) {
        this.game = game;

        // if areGridLinesVisible, show the grid lines
        this.gridLinesVisibleProperty().bind(UIConfig.mapGridVisible);
        this.cells = new CellUI[Map.HEIGHT][Map.WIDTH];

        // create and add all CellUI of the game
        for (int y = 0; y < Map.HEIGHT; y++) {
            for (int x = 0; x < Map.WIDTH; x++) {
                CellUI cell = new CellUI(new Position(x, y), game);
                cells[y][x] = cell;
            }
            this.addRow(y, this.cells[y]);
        }

        Position playerPos = this.game.getPlayer().getPosition();
        this.setSelectedCell(this.cells[playerPos.getY()][playerPos.getX()]);
    }

    /**
     * Returns the cellUI of the mapUI at a given position.
     *
     * @param pos The position of the cellUI.
     * @return The cellUI.
     */
    public CellUI getCellUI(Position pos) {
        return this.cells[pos.getY()][pos.getX()];
    }

    public void setSelectedCell(CellUI toHighlight) {
        if (this.selectedCell != null) {
            this.selectedCell.setSelected(false);
        }
        this.selectedCell = toHighlight;
        this.selectedCell.setSelected(true);
    }

    /**
     * Returns the selected cellUI.
     *
     * @return the selected cell
     */
    public CellUI getSelectedCellUI() {
        return this.selectedCell;
    }
}
