package langfall.view.components;

import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import langfall.events.CellSelectedEvent;
import langfall.exceptions.EmptyCellException;
import langfall.exceptions.OccupiedCellException;
import langfall.model.Game;
import langfall.model.Player.Status;
import langfall.model.land.FarmableLand;
import langfall.model.map.Cell;
import langfall.model.utils.Position;
import langfall.view.ImageLoader;

/**
 * Class representing the UI component Cell
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class CellUI extends StackPane {

    /**
     * The land's image.
     */
    private ImageView landImageView;

    /**
     * The building's image.
     */
    private ImageView buildingImageView;

    /**
     * A transparent image of the building.
     */
    private ImageView buildingTransparentImageView;

    /**
     * The player's image.
     */
    private ImageView playerImageView;

    /**
     * The cell's tooltip.
     */
    private Tooltip tooltip;

    /**
     * The height of a cellUI
     */
    public static int HEIGHT = 40;

    /**
     * The width of a cellUI
     */
    public static int WIDTH = 40;

    /**
     * The game.
     */
    private final Game game;

    /**
     * The corresponding Cell
     */
    private final Cell cell;

    /**
     * A highlighted rectangle to represent a selected cell.
     */
    private final Rectangle highlightedCell;

    /**
     * Indicates whether this cell is selected.
     */
    private boolean selected;

    /**
     * Constructor: initializes a cellUI with its position and the game.
     *
     * @param pos  the pos of the cell
     * @param game the game
     */
    public CellUI(Position pos, Game game) {
        this.game = game;
        this.cell = game.getMap().getCell(pos);
        this.addMouseListener();
        this.update();

        this.selected = false;

        this.highlightedCell = new Rectangle();
        this.highlightedCell.setWidth(CellUI.WIDTH);
        this.highlightedCell.setHeight(CellUI.HEIGHT);
        this.highlightedCell.setOpacity(0.25);
        this.highlightedCell.setFill(Color.WHITE);
        this.highlightedCell.setVisible(this.selected);

        this.getChildren().add(this.highlightedCell);
    }

    public Cell getCell() {
        return this.cell;
    }


    /**
     * Returns if the cell is selected.
     *
     * @return true if the cell is selected, false otherwise
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets if the cell is selected and highlights it if it is.
     *
     * @param selected if the cell is selected
     */
    public void setSelected(boolean selected) {
        highlightedCell.toFront();
        this.selected = selected;
        highlightedCell.setVisible(selected);
    }

    /**
     * Returns the building image of this cell.
     *
     * @return the building image
     */
    private Image getBuildingImage() {
        return getBuildingImage(this.cell);
    }

    /**
     * Returns the building image of the cell.
     *
     * @param cell the cell
     * @return the building image
     */
    private Image getBuildingImage(Cell cell) {
        return ImageLoader.getImage(cell.getBuilding().getName());
    }

    /**
     * Creates a contextual menu for the cell.
     *
     * @return a contextual menu
     */
    public ContextMenu createContextMenu() {
        ContextMenu contextMenu = new ContextMenu();
        // contextMenu.setStyle("-fx-background:black");

        if (this.cell.getBuilding() != null) {
            MenuItem item1 = new MenuItem("Destroy");
            item1.setOnAction(click -> {
                try {
                    this.game.getPlayer().destroyBuilding(this.cell);
                } catch (EmptyCellException e) {
                    e.printStackTrace();
                }
            });

            MenuItem item2 = new MenuItem("Move");
            item2.setOnAction(click -> {
                try {
                    this.game.getPlayer().startMovingBuilding(this.cell);
                } catch (EmptyCellException e) {
                    e.printStackTrace();
                }
            });

            // Add MenuItem to ContextMenu
            contextMenu.getItems().addAll(item1, item2);
        }
        return contextMenu;
    }

    /**
     * Updates the cell containing the player.
     */
    public void updatePlayer() {
        if (playerImageView != null) {
            this.getChildren().remove(playerImageView);
            this.playerImageView = null;
        }

        /* Update Player ImageView  of the cellUI */
        //  if cell is pos player, add player to cell
        if (game.getPlayer().getPosition().equals(this.cell.getPos())) {
            // create
            playerImageView = new ImageView(getPlayerImage());
            // config
            playerImageView.setPreserveRatio(true);
            playerImageView.setFitHeight(CellUI.HEIGHT * 0.9);
            playerImageView.setFitWidth(CellUI.WIDTH * 0.9);
            // add to stack
            this.getChildren().add(playerImageView);
            setAlignment(playerImageView, Pos.BOTTOM_CENTER);
        }
    }

    /**
     * Updates the cell's land.
     */
    public void updateLand() {
        /* Update Land ImageView of the cellUI */
        //  create
        landImageView = new ImageView(getLandImage());
        //  config
        landImageView.setFitHeight(CellUI.HEIGHT);
        landImageView.setFitWidth(CellUI.WIDTH);
        //  add to stack
        this.getChildren().add(landImageView);

        if (cell.getLand() instanceof FarmableLand) {
            FarmableLand land = (FarmableLand) cell.getLand();

            // tooltip setup
            String tooltipText;
            if (land.getMaxDurability() == -1) {
                tooltipText =
                    land.getName() + "\n" + "\u221E";
            } else {
                tooltipText =
                    land.getName() + "\n" + land.getDurability() + "/" + land.getMaxDurability();
            }
            tooltip = new Tooltip(tooltipText);
            tooltip.setShowDelay(Duration.millis(10));
            tooltip.setHideDelay(Duration.millis(10));
            tooltip.setTextAlignment(TextAlignment.CENTER);

            Tooltip.install(landImageView, tooltip);
        }
    }

    /**
     * Updates the cell's building.
     */
    public void updateBuilding() {
        if (cell.getBuilding() != null) {
            //  create
            buildingImageView = new ImageView(getBuildingImage());
            //  config
            buildingImageView.setFitHeight(CellUI.HEIGHT);
            buildingImageView.setFitWidth(CellUI.WIDTH);
            //  add to stack
            this.getChildren().add(buildingImageView);
        } else {
            this.getChildren().remove(buildingImageView);
        }
    }

    /**
     * Display a transparent image of the building to help placing it.
     *
     * @param display whether the transparent image of the building should be displayed on this cell
     *                or not
     */
    public void toggleBuildingPattern(boolean display) {
        if (display) {
            //  create
            buildingTransparentImageView = new ImageView(
                getBuildingImage(game.getPlayer().getSelectedCell()));
            //  config
            buildingTransparentImageView.setFitHeight(CellUI.HEIGHT);
            buildingTransparentImageView.setFitWidth(CellUI.WIDTH);
            // image view effect
            buildingTransparentImageView.setOpacity(0.7);
            //  add to stack
            this.getChildren().add(buildingTransparentImageView);
        } else {
            this.getChildren().remove(buildingTransparentImageView);
        }
    }

    /**
     * Update the cell from the data in the game.
     */
    public void update() {
        // clear the old data of the cellUI
        this.getChildren().clear();
        updateLand();
        updateBuilding();
        updatePlayer();
        /* ------------------ !!!! Don't add other think after player !!! ------------------ */
    }

    /**
     * Returns the image of the player according to his direction.
     *
     * @return the image of the player
     */
    private Image getPlayerImage() {
        switch (game.getPlayer().getDirection()) {
            case NORTH:
                return ImageLoader.getImage("Player_NORTH");
            case WEST:
                return ImageLoader.getImage("Player_WEST");
            case EAST:
                return ImageLoader.getImage("Player_EAST");
            default:
                return ImageLoader.getImage("Player_SOUTH");
        }
    }

    /**
     * Returns the image of the cell's land.
     *
     * @return the image of the cell's land
     */
    private Image getLandImage() {
        if (cell.getLand() instanceof FarmableLand) {
            FarmableLand land = (FarmableLand) cell.getLand();
            if (land.getDurability() == 0 && land.isPassable()) {
                return ImageLoader.getImage("Grass");
            }
        }
        return ImageLoader.getImage(cell.getLand().getName());
    }

    /**
     * Manages action depending of if it's a right or left click.
     */
    public void addMouseListener() {
        this.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                if (game.getPlayer().getState() == Status.movingABuilding) {
                    try {
                        game.getPlayer().moveBuilding(this.cell);
                        this.setSelected(false);
                        toggleBuildingPattern(false);
                    } catch (OccupiedCellException e) {
                        // do nothing
                    }
                } else {
                    this.setSelected(true);
                    this.game.dispatchEvent(new CellSelectedEvent(this.cell.getPos()));
                }
            } else if (event.getButton() == MouseButton.SECONDARY) {
                ContextMenu contextMenu = createContextMenu();
                contextMenu.show(this, event.getScreenX(), event.getScreenY());
                this.setSelected(false);
            }
        });
        this.setOnMouseEntered(event -> {
            if (game.getPlayer().getState() == Status.movingABuilding) {
                toggleBuildingPattern(true);
            }
        });

        this.setOnMouseExited(event -> {
            if (game.getPlayer().getState() == Status.movingABuilding) {
                toggleBuildingPattern(false);
            }
        });
    }
}
