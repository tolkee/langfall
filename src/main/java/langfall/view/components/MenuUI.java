package langfall.view.components;


import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import langfall.model.BackupsRepository;
import langfall.model.Game;
import langfall.view.ImageLoader;
import langfall.view.Langfall;
import langfall.view.UIConfig;

/**
 * Class representing the menu bar of the application.
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class MenuUI extends MenuBar {

    /**
     * Constructor: initializes the application menu.
     */
    public MenuUI(Game game) {
        //create menus
        Menu gameMenu = new Menu("Game");
        Menu settingsMenu = new Menu("Settings");
        settingsMenu.setGraphic(new ImageView(ImageLoader.getImage("Settings")));

        // create menusItems
        //      settingsMenu items
        MenuItem showGridItem = new MenuItem();
        showGridItem.textProperty()
            .bind(Bindings.when(UIConfig.mapGridVisible).then("Hide Grid").otherwise("Show Grid"));
        //      controls items
        MenuItem controlsItem = new MenuItem("Controls");

        //      homeMenu items
        MenuItem homeItem = new MenuItem("Home");
        MenuItem saveItem = new MenuItem("Save");
        saveItem.setGraphic(new ImageView(ImageLoader.getImage("Save")));

        // set accelerators
        //      settingsMenu accelerators
        showGridItem.setAccelerator(KeyCombination.keyCombination("Ctrl+L"));
        //      homeMenu items

        // set listeners
        //      settingsMenu listeners
        showGridItem
            .setOnAction(event -> UIConfig.mapGridVisible.set(!UIConfig.mapGridVisible.get()));
        controlsItem.setOnAction(event -> UIConfig.controlsUI.show());

        //      homeMenu listeners
        homeItem.setOnAction(event -> {
            // stop the current game
            game.stop();

            // save the current game
            try {
                BackupsRepository.saveBackup(game);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // change to home scene
            Langfall.showHome((Stage) this.getScene().getWindow());
        });

        saveItem.setOnAction(event -> {
            try {
                BackupsRepository.saveBackup(game);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        // add menuItems to the menus
        //      settingsMenu
        settingsMenu.getItems().addAll(showGridItem, controlsItem);
        //      homeMenu
        gameMenu.getItems().addAll(homeItem, saveItem);

        // add menus to menuBar
        this.getMenus().addAll(gameMenu, settingsMenu);
    }
}
