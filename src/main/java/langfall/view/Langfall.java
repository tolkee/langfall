package langfall.view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import langfall.model.Game;
import langfall.view.scenes.HomeScene;
import langfall.view.stages.GameStage;

/**
 * Class representing the Application JavaFX of Langfall
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class Langfall extends Application {

    /**
     * The main window.
     */
    public static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        Langfall.primaryStage = primaryStage;
        primaryStage.getIcons().add(ImageLoader.getImage("Logo"));
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

        // set the scene
        primaryStage.setScene(new HomeScene());
        primaryStage.show();
    }

    /**
     * Displays the game stage
     *
     * @param game the game
     */
    public static void showGame(Game game) {
        Langfall.primaryStage.close();
        GameStage gameStage = new GameStage(game);
        gameStage.show();
    }

    /**
     * Displays the home scene.
     *
     * @param currentStage the current stage
     */
    public static void showHome(Stage currentStage) {
        currentStage.close();
        Langfall.primaryStage.setScene(new HomeScene());
        Langfall.primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
