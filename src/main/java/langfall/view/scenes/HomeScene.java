package langfall.view.scenes;

import java.io.IOException;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import langfall.exceptions.InvalidTemplateException;
import langfall.model.BackupsRepository;
import langfall.model.Game;
import langfall.view.ImageLoader;
import langfall.view.Langfall;

/**
 * Class representing the home scene of Langfall
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class HomeScene extends Scene {

    /**
     * The selected backup of the list
     */
    String selectedBackup = "";

    /**
     * Creates and loads the home scene.
     */
    public HomeScene() {
        super(new VBox());

        Langfall.primaryStage.setTitle("Langfall");

        // setup the scene
        VBox root = (VBox) this.getRoot();
        root.setPrefHeight(500);
        root.setPrefWidth(750);
        root.setAlignment(Pos.TOP_CENTER);
        root.setBackground(new Background(
            new BackgroundImage(ImageLoader.getImage("Background"), BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                BackgroundSize.DEFAULT)));

        // set up title
        Label title = new Label(
            "\uD835\uDD77\uD835\uDD86\uD835\uDD93\uD835\uDD8C\uD835\uDD8B\uD835\uDD86\uD835\uDD91\uD835\uDD91");
        title.setTextFill(Color.WHITE);
        title.setStyle("-fx-font-size: 70; -fx-font-weight: bold");

        // set up backups list
        ListView<String> backups = new ListView<>();
        backups.setStyle(
            "-fx-background-color: rgba(255, 255, 255, 0.2);");
        backups.setFixedCellSize(50);
        backups.setOnMouseClicked(
            event -> {
                String selectedItem = backups.getSelectionModel().getSelectedItem();
                if (selectedItem != null) {
                    this.selectedBackup = selectedItem;
                }
            });
        backups.setItems(FXCollections.observableArrayList(BackupsRepository.getAllBackupsName()));

        // set up buttons
        //      Buttons container
        HBox buttons = new HBox();
        buttons.setAlignment(Pos.CENTER);
        buttons.setSpacing(20);
        buttons.setPadding(new Insets(15, 0, 15, 0));

        //              new game button
        HBox newBox = new HBox();

        // textfield for game title that only accept alphanumeric
        TextField newInput = new TextField();
        Pattern pattern = Pattern.compile("([a-zA-Z0-9]*[ ]*)+");
        newInput.setTextFormatter(new TextFormatter<String>(change -> {
            if (pattern.matcher(change.getControlNewText()).matches()) {
                return change;
            } else {
                return null;
            }
        }));
        newInput.setPromptText("Enter the new game");
        newInput.setPrefHeight(70);

        Button newButton = createButton("New Game", event -> {
            String gameTitle = newInput.getText().trim();
            if (!gameTitle.equals("")) {
                try {
                    Game game = new Game(gameTitle, "map1.json");
                    Langfall.showGame(game);
                } catch (InvalidTemplateException e) {
                    e.printStackTrace();
                }
            }
        });
        newButton.setGraphic(new ImageView(ImageLoader.getImage("Add")));
        newBox.getChildren().addAll(newButton, newInput);

        //              load game button
        Button loadButton = createButton("Load Game", event -> {
            if (!this.selectedBackup.equals("")) {
                try {
                    Game game = BackupsRepository.loadBackup(this.selectedBackup);
                    Langfall.showGame(game);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        loadButton.setGraphic(new ImageView(ImageLoader.getImage("Download")));

        //              delete game button
        Button deleteButton = createButton("Delete", event -> {
            if (!this.selectedBackup.equals("")) {
                BackupsRepository.deleteBackup(this.selectedBackup);
                backups
                    .setItems(
                        FXCollections.observableArrayList(BackupsRepository.getAllBackupsName()));
                this.selectedBackup = "";
            }
        });
        deleteButton.setGraphic(new ImageView(ImageLoader.getImage("Delete")));

        // add all buttons to the buttons container
        buttons.getChildren().addAll(newBox, loadButton, deleteButton);

        // add all elements to the scene
        root.getChildren().addAll(title, backups, buttons);
    }

    /**
     * Create a javafx button with an handler.
     *
     * @param text    the text of the button
     * @param handler the action handler of the button
     * @return the created button
     */
    private Button createButton(String text, EventHandler<ActionEvent> handler) {
        Button newButton = new Button(text);
        newButton.setPrefHeight(70);
        newButton.setTextFill(Color.WHITE);
        newButton.setStyle("-fx-background-color: #202426;");
        newButton.setCursor(Cursor.OPEN_HAND);
        newButton.setOnAction(handler);
        return newButton;
    }
}
