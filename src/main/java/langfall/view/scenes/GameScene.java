package langfall.view.scenes;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import langfall.events.BuildingModifiedEvent;
import langfall.events.CellSelectedEvent;
import langfall.events.InventoryEvent;
import langfall.events.LandModifiedEvent;
import langfall.events.PlayerMovedEvent;
import langfall.model.Game;
import langfall.model.building.Building;
import langfall.model.utils.Direction;
import langfall.view.UIConfig;
import langfall.view.components.BuildingUI;
import langfall.view.components.CellUI;
import langfall.view.components.MapUI;
import langfall.view.components.MenuUI;
import langfall.view.components.info.InfoUI;
import langfall.view.stages.ControlsStage.Controls;

/**
 * Class representing the game scene of Langfall
 *
 * @author Guillaume Lacoste
 * @version 2.0
 */

public class GameScene extends Scene {

    /**
     * Creates and initializes the game scene for the game.
     *
     * @param game the game
     */
    public GameScene(Game game) {
        super(new BorderPane());

        // create root layer
        BorderPane root = (BorderPane) this.getRoot();
        root.setStyle("-fx-background-color: #121212;");

        // create menu component
        MenuUI menu = new MenuUI(game);

        // create map component
        MapUI map = new MapUI(game);

        // create infos component
        InfoUI info = new InfoUI(game);

        // create building component
        BuildingUI building = new BuildingUI(game.getPlayer(), map.getSelectedCellUI().getCell());

        // add components to the root layer
        root.setTop(menu);
        root.setCenter(map);
        root.setRight(info);
        root.setBottom(building);

        // set game scene key listeners
        this.setOnKeyPressed(event -> {
            KeyCode key = event.getCode();
            if (key == UIConfig.controlsUI.getControl(Controls
                .FARM)) {
                game.getPlayer().farm();
            } else if (key == UIConfig.controlsUI.getControl(Controls
                .BUILD)) {
                Class<? extends Building> buildingClass = building.getSelectedBuildingClass();
                game.getPlayer().build(buildingClass);
            } else if (key == UIConfig.controlsUI.getControl(Controls
                .UP)) {
                game.getPlayer().move(Direction.NORTH);
            } else if (key == UIConfig.controlsUI.getControl(Controls
                .DOWN)) {
                game.getPlayer().move(Direction.SOUTH);
            } else if (key == UIConfig.controlsUI.getControl(Controls
                .LEFT)) {
                game.getPlayer().move(Direction.WEST);
            } else if (key == UIConfig.controlsUI.getControl(Controls
                .RIGHT)) {
                game.getPlayer().move(Direction.EAST);
            }
        });

        // game events listeners
        //      player moves listener
        game.addEventListener(PlayerMovedEvent.class, event -> Platform.runLater(() -> {
            CellUI fromCellUI = map.getCellUI(event.from);
            fromCellUI.updatePlayer();

            CellUI toCellUI = map.getCellUI(event.to);
            toCellUI.updatePlayer();

            // disable craftTool button if player is not on the selected building
            if (toCellUI.isSelected()) {
                info.updateCellInfo(toCellUI.getCell());
            } else if (fromCellUI.isSelected()) {
                info.updateCellInfo(fromCellUI.getCell());
            }
        }));

        //      land changes listener
        game.addEventListener(LandModifiedEvent.class, event -> Platform.runLater(() -> {
            CellUI cellUI = map.getCellUI(event.pos);
            cellUI.updateLand();

            if (cellUI.isSelected()) {
                info.updateCellInfo(cellUI.getCell());
                cellUI.setSelected(true);
            }
        }));

        //  selected cell listener
        game.addEventListener(CellSelectedEvent.class, event -> Platform.runLater(() -> {
            CellUI selectedCellUI = map.getCellUI(event.pos);
            info.updateCellInfo(selectedCellUI.getCell());
            building.updateBuildings(selectedCellUI.getCell());
            //highlight the selected cell and unlight the older one
            map.setSelectedCell(selectedCellUI);
        }));

        //      building changes listener
        game.addEventListener(BuildingModifiedEvent.class, event -> Platform.runLater(() -> {
            map.getCellUI(event.pos).updateBuilding();
            info.updatePlayerInfo();
        }));

        // inventory changed listener
        game.addEventListener(InventoryEvent.class, event -> Platform.runLater(() -> {
            info.updatePlayerInfo();
            building.updateRecipe();
        }));
    }
}
