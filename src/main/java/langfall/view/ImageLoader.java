package langfall.view;

import java.util.HashMap;
import java.util.Map;
import javafx.scene.image.Image;

/**
 * Class loading all the images used in Lanfall Application.
 * <p>
 * !!!! Don't load images other than in the ImageLoader !!!!
 * <p>
 * To use an image on the application, first put your image file (.png, .jpg etc...) on the
 * resources repository of main. <br> Then add loadImage("ImageName",  "imageFileName") to the
 * ImageLoader, don't forget the file extension in the imageFileName. <br> Now you can do from
 * everywhere in the application ImageLoader.getImage("ImageName") to get your image.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class ImageLoader {

    /**
     * The loaded images
     */
    private static Map<String, Image> loadedImages = new HashMap<>();

    static {
        // Load land images
        loadImage("Grass", "lands/grass.jpg");
        loadImage("Forest", "lands/forest.jpg");
        loadImage("Mountain", "lands/mountain.jpg");
        loadImage("River", "lands/water.jpg");

        // Load building images
        loadImage("Forge", "buildings/forge.png");
        loadImage("Sawmill", "buildings/sawmill.png");
        loadImage("Workshop", "buildings/workshop.png");
        loadImage("Bridge", "buildings/bridge.png");

        // Load elements images
        loadImage("WOOD", "elements/wood.png");
        loadImage("ORE", "elements/ore.png");
        loadImage("WATER", "elements/water.png");
        loadImage("BOARD", "elements/board.png");
        loadImage("GOLDBAR", "elements/goldbar.png");
        loadImage("GOLDENBOARD", "elements/goldenboard.png");
        loadImage("BRIDGE", "buildings/bridge.png");

        // Load player images
        loadImage("Player_SOUTH", "player/player_south.png");
        loadImage("Player_NORTH", "player/player_north.png");
        loadImage("Player_WEST", "player/player_west.png");
        loadImage("Player_EAST", "player/player_east.png");

        // Load Icons
        loadImage("Craft", "icons/craft.png");
        loadImage("Download", "icons/download.png");
        loadImage("Add", "icons/add.png");
        loadImage("Delete", "icons/delete.png");
        loadImage("Settings", "icons/settings.png");
        loadImage("Home", "icons/home.png");
        loadImage("Save", "icons/save.png");
        loadImage("Player", "icons/player.png");

        loadImage("Background", "background.png");
        loadImage("Logo", "logo.png");

    }

    /**
     * Get the image related to the given name from the loaded images.
     *
     * @param image The name representing the image.
     * @return The image.
     */
    public static Image getImage(String image) {
        return loadedImages.get(image);
    }

    /**
     * Load the image of the given filename.
     *
     * @param image    The name representing the image.
     * @param fileName The name of the image file (with the file extension)
     */
    private static void loadImage(String image, String fileName) {
        loadedImages.put(image,
            new Image(ImageLoader.class.getResourceAsStream("/images/" + fileName)));
    }

}
