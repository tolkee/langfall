package langfall.events;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class representing a dispatcher of events.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public abstract class Dispatcher {

    /**
     * Map of event types to be dispatched, associated with a list of their listeners.
     */
    private transient HashMap<Class<? extends Event>, ArrayList<Listener>> listeners;

    /**
     * Constructor : intializes the dispatcher.
     */
    protected Dispatcher() {
        this.init();
    }

    /**
     * Initializes the dispatcher.
     */
    public void init() {
        listeners = new HashMap<>();
    }

    /**
     * Adds a listener to a type of event dispatched by the dispatcher.
     *
     * @param eventType the class of the event to listen
     * @param listener  the listener
     * @param <T>       the type of the event
     */
    public <T extends Event> void addEventListener(Class<T> eventType, Listener<T> listener) {
        var listeners = this.listeners.getOrDefault(eventType, new ArrayList<>());
        listeners.add(listener);
        this.listeners.put(eventType, listeners);
    }

    /**
     * Dispatch an event to his listeners.
     *
     * @param event the Event to dispatch
     * @param <T>   the type of the event
     */
    @SuppressWarnings("unchecked")
    public <T extends Event> void dispatchEvent(T event) {
        var listeners = this.listeners.get(event.getClass());
        if (listeners == null) {
            return;
        }
        for (var listener : listeners) {
            listener.accept(event);
        }
    }
}