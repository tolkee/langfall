package langfall.events;

import java.util.function.Consumer;

/**
 * Interface representing an event listener.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public interface Listener<T extends Event> extends Consumer<T> {

}
