package langfall.events;

import langfall.model.utils.Position;

/**
 * Class representing a modified land event.
 *
 * @author Blasius Marine
 * @version 1.0
 */

public class LandModifiedEvent implements Event {

    /**
     * The modified land's position.
     */
    public final Position pos;

    /**
     * Constructor: Initialize the event with the modified land's position.
     *
     * @param pos The position of the modified land
     */
    public LandModifiedEvent(Position pos) {
        this.pos = pos;
    }
}
