package langfall.events;

import langfall.model.utils.Position;

/**
 * Class representing a selected cell event.
 *
 * @author Blasius Marine
 * @version 1.0
 */

public class CellSelectedEvent implements Event {

    /**
     * The modified cell's position.
     */
    public final Position pos;

    /**
     * Constructor: initializes the event with the modified land's position.
     *
     * @param pos the position of the modified land
     */
    public CellSelectedEvent(Position pos) {
        this.pos = pos;
    }
}