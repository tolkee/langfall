package langfall.events;

import langfall.model.utils.Position;

/**
 * Class representing a modified building event.
 *
 * @author Blasius Marine
 * @version 1.0
 */

public class BuildingModifiedEvent implements Event {

    /**
     * The modified building's position.
     */
    public final Position pos;

    /**
     * Constructor: initializes the event with the modified building's position.
     *
     * @param pos the position of the modified building
     */
    public BuildingModifiedEvent(Position pos) {
        this.pos = pos;
    }

}
