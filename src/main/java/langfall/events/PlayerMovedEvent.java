package langfall.events;

import langfall.model.utils.Position;

/**
 * Class representing a moving player event.
 *
 * @author Guillaume Lacoste
 * @version 1.0
 */

public class PlayerMovedEvent implements Event {

    /**
     * The player's position before moving.
     */
    public final Position from;

    /**
     * The player's position after moving.
     */
    public final Position to;

    /**
     * Constructor: Initialize the event with the player's positions, before and after moving.
     *
     * @param from the player's position before moving
     * @param to   the player's position after moving
     */
    public PlayerMovedEvent(Position from, Position to) {
        this.from = from;
        this.to = to;
    }
}
